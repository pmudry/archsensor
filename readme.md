## Introduction
This readme guides you through the setup the iNUIT gateway software stack and getting started with. The iNUIT gateway can be used in 
order to wire sensors and actuators together and publish some information (notable their model and data samples) to a cloud platform.

For more information about the project itself we refer to the documentation of the ArchSensor and iNUIT projects.

*All procedures were tested on Unbuntu 14.04.2 LTS Desktop 64 bit, it should work on other platforms too. If you try to install and run 
the software on the Raspberry PI, have a look at the file [raspberrypi.md](raspberrypi.md)*

## Install requirements
In order to be able to compile and run the iNUIT gateway software stack, some tools and libraries need to be installed on the development
host. Basically the Java Development Kit (JDK) and the build tool Maven are required among other optional tools.

### Java SDK 8
 
    # sudo add-apt-repository ppa:webupd8team/java
    # sudo apt update
    # sudo apt install oracle-java8-installer libunixsocket-java
    # sudo cp /usr/lib/jni/libunix-java.so /usr/lib/

### Maven 3

    # sudo apt update
    # sudo apt install maven

### Install GIT (optional)
GIT client is only needed if you want to use the SCM features or contribute to the project, otherwise you can go to the project's 
bitbucket [page](https://bitbucket.org/pmudry/archsensor) (It is highly probable that you are already on that page) of the project and 
download the master branch as a zip file and extract that file to your PC.

    # sudo apt update
    # sudo apt install git    
    # git config --global user.name "{first name} {last name}"
    # git config --global user.email "{email}"

### Install Mosquitto MQTT Broker (optional)
The iNUIT gateway can work either with a local MQTT broker or with a MQTT broker on any other machine. So if
you will work with a remote MQTT broker you can skip this step.

    # sudo apt update
    # sudo apt install mosquitto

Note that the installation script actually starts the Mosquitto MQTT broker in the background and on boot,
so the broker will be available instantly after having been installed.

#### Graphical MQTT client
There exist a number of graphical MQTT clients which can be used in order to monitor the messages passed between the 
different actors on the MQTT broker. We used most of the time [mqtt-spy](http://kamilfb.github.io/mqtt-spy/), which
works for the most debugging tasks quite well. If you like it you can even donate money to unicef....

### Install and run Node-Red (optional)
Node red can be used to define local control and regulation algorithms (or even remote by connecting to a remote MQTT broker) by using a 
very simple web interface. The implementation of the nodes itself is not very convenient, as the messages contain JSON content, so each
node has to parse the actual data and the user has to write those functions in Javascript. But it is a prove of concept addition to the 
gateway and shows the potential of IoT installations in general.

    # sudo apt update
    # sudo apt install nodejs nodejs-legacy npm
    # sudo npm install -g node-red
    # node-red

Node-Red prints the port to connect to on the console when launching. Add a MQTT broker to the configuration 
on the web interface: **localhost:1883**. Now you can use Node-Red in order to send and receive MQTT messages
and interact with the iNUIT gateway components.

## Build and run the iNUIT gateway software stack

### Checkout sources from bitbucket

    # git clone https://{user}@bitbucket.org/pmudry/archsensor.git

### Download sources from bitbucket
Alternatively you can download the sources directly from the website:

    # wget https://bitbucket.org/pmudry/archsensor/get/master.tar.bz2 --http-user={user} --http-password={password}
    # tar -xjf master.tar.bz2

### Use maven to build and start the OSGi based iNUIT gateway in a Felix OSGi container
Change into the project root folder and type

    # cd inuit-osgi
    # mvn    

The Maven project sets everything up and starts all OSGi bundles in an Apache Felix OSGi container. If you see 
the Felix console like this:

    ____________________________
    Welcome to Apache Felix Gogo
    
    g!

...all bundles were compiled successfully and the gateway is actually running. You can use the standard Felix OSGi
console commands to inspect the installed bundles and control them.

- `lb`: lists the installed bundles and their state.
- `stop`: Stops the bundle with the given id. `stop 0` shuts the OSGi container down.

### Export runnable zip distribution of the actual configuration

It is very simple do create a distribution of the actual project using the following command:

    # mvn -Dexecutor=zip
    
This will create a file named **paxrunner-runner.zip** inside the root project folder. You can distribute this ZIP 
file and extract it on the target. The extracted folder contains a **start.sh** (for windows a **start.bat**) file
which starts the gateway software. Note that event the configuration of the OSGi environment is taken from the maven project and
integrated into the generated zip file.

### Set configuration options for bundles
You might want to set configuration options for the bundles actually running inside the OSGi container. This is no
problem. Just edit the file **pom.xml** in the project root directory *(inuit-osgi)* and add your options inside the
<properties> tags.

Example:

    <!-- The properties here will be copied to the OSGi configuration file. -->
    <properties>
        <org.mycoolbundle.option1>{value}</org.mycoolbundle.option1>
    </properties>

And those entries will be automatically added to the OSGi configuration file bundled with the OSGi container.

### Start project inside another OSGi container
Using PAX it is quite simple to test the application inside another OSGi container:

    # mvn -Dframework=equinox

The command above starts the OSGi bundles inside a Equinox OSGi container. We refer to the PAX documentation for 
other supported frameworks.

## Develop for the iNUIT gateway

### Maven goals
The default maven goal compiles the complete iNUIT project, installs all OSGi bundles and Jar wrappers and starts the OSGi container. There
are other maven goals in order to do just a part of this tasks.

#### `mvn compile`
Compiles all OSGi bundles of the project.

#### `mvn compile -pl {bundle}`
Will compile only the given OSGi bundle.

#### `mvn install`
Compiles and installs all OSGi bundles of the project to the embedded OSGi runtime.

#### `mvn install -pl {bundle}`
Will compile and install only the given OSGi bundle.

There exists maven goals to install new OSGi modules or create new projects. These are threaten in the few next chapters...

### Import an existing OSGi bundle from maven repository
In the case you need to add an existing OSGi bundle to your project and the bundle exists as a maven artifact, you can add the bundle with
a single line to the project:

    # mvn pax:import-bundle -DgroupId={group id} -DartifactId={artifact} -Dversion={version}

and you are done. The bundle has been added to the OSGi runtime and will be started automatically the next time you start the iNUIT 
gateway using the command `mvn`. 

### Wrap an existing Maven Java library (from maven central) into a OSGi bundle
For the case multiple OSGi bundles requires a Java library (JAR file) to be present in the OSGi environment, there is a very simple 
command in order to wrap a simple Java library into an OSGi bundle and add that bundle to the project:

    # mvn pax:wrap-jar -DgroupId={group id} -DartifactId={artifact} -Dversion={version}

This will download the artifact using maven, wraps it into an OSGi bundle and adds the bundle to the OSGi environment.

**If only one bundle depends on a JAR, user rather pax:emebed-jar to embed the jat into the bundle.**

### Add a new bundle to the project
The maven PAX plugin offers a maven goal in order to create a new OSGi bundle in the context of the current project by issuing the 
following goal with some parameters:

    # mvn pax:create-bundle -Dpackage={package} -Dname={name} -Dversion={version}

This will create a folder named after the package and the folder contains a ready-to-use OSGi bundle project. All is already set up, so by
issuing the default maven goal by typing:

    # mvn

... your new bundle (skeleton) will be compiled, installed to the OSGi runtime and started automatically. 

The generated bundle is an empty OSGi bundle project with the following structure (Example package is "org.example":

    org.example
    ├── osgi.bnd
    ├── pom.xml
    └── src
        └── main
            ├── java
            │   └── org
            │       └── example
            │           ├── ExampleService.java
            │           └── internal
            │               ├── ExampleActivator.java
            │               └── ExampleServiceImpl.java
            └── resources
                └── readme.txt

The file **sgi.bnd** contains the OSGi metadata in order to construct the **MANIFEST** file for the bundle.

The **pom.xml** is the maven project file for the OSGi bundle and defines all dependencies and project properties.

A standard maven project structure is created without test folder.

Two packages are created: **org.example** and **org.example.internal** whereas the first is public and exported by the bundle and the 
second is not exported. The public package contains the interfaces of the services provided by the package and the internal package 
contains the implementation of the services and the bundle activator.

The Java source file **ExampleService.java** contains an example interface which is implemented by the **ExampleServiceImpl.java** inside
the internal package. The file **ExampleActivator.java** contains the code to actually register the OSGi bundle within the container and
publish the implemented services.

If you prefer to compile the actual bundle only you can either change to the bundle's folder and issue `mvn compile` or `mvn install` to 
install the bundle. In the root folder, you can specify the bundle subject using the `-pl` option.

Example:

    mvn compile -pl {bundle}

### Add local bundle dependency
Edit the project's **pom.xml** and add:

    <dependency>
      <groupId>ch.hevs.inuit.osgi</groupId>
      <artifactId>{artifact id}</artifactId>
      <version>{version}</version>
    </dependency>
    
to the dependencies list of your bundle.

### Embed maven artifact into bundle

If only a single bundle has a dependency to a maven artifact, the artifact can be directly embedded into the bundle itself. All you have
to do is to change into the folder of the bundle and type:

    mvn pax:embed-jar -DgroupId={group id} -DartifactId={artifact} -Dversion={version}
    
and the maven PAX plugin will add the maven dependency to the bundle's pom.xml file and make the needed changes in the bundle manifest
file.

### Debugging your OSGi bundle
PAX starts the OSGi container in a separate process, so debugging the maven goal does not work. You have basically two possibilities:

#### Start Felix manually
Ensure that the maven bundles and configuration are up to date by issuing:

    # mvn install pax:provision -Dexecutor=script
    
Then you can use your preferred Java IDE or Java debugger and start Felix as follows:
    
#### Use Java remote debugging
To enable remote debugging you will have to:

* Add the following option when starting Pax Runner:

    --vmOptions="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
    
* In your IDE of debugger start a remote debugging session on:

    host: localhost (or host/ip of the machine that runs Pax Runner if is Pax Runner and debugger do not run on the same machine)
    port: 5005 (or any port but then change also the options above)

## iNUIT bundles
### ch.hevs.inuit.library
This OSGi bundle contains:

* The very basic interfaces of multiple iNUIT gateway services.
* Some common helper classes for other iNUIT based OSGi bundles/services.
* The JSON message definitions.

### ch.hevs.inuit.directory
Implements the IDirectroy interface/service:

The IDirectory service acts as a global key/value storage for all the OSGi bundles on the iNUIT gateway platform.

### ch.hevs.inuit.presencemanager
Implements the IPresenceManager interface/service:

The IPresenceManager service acts as a global publish/subscribe office where device drivers can announce the presence of new equipment or
register themselves in order to be informed about those announcements.

### ch.hevs.inuit.presencefilereader
Bootstraps the gateway using a JSON file that actually contains the configuration in JSON format. The configuration file will be searched
at the following locations (in the given sequence):

* If a property **ch.hevs.inuit.presencefilereader.file** is present, the bootstrapper tries to open the file at the given location.
* If the property is not present or the file does not exists, the file **.config/inuit/bootstrap.json** inside the home directory of the 
  current user will be used.
* If the file above does not exist, the bootstrapper tries to use the file **/etc/inuit/bootstrap.json**.
* If the global bootstrap file does not exist, the bootstrapper will use the file **bootstrap.json** inside the OSGi bundle as last 
  alternative.
* If even the file inside the OSGi bundle is missing the bootstrapper fails and does nothing at all.

### ch.hevs.inuit.messaging
Offers the ability to connect to one or more MQTT brokers using either MQTT or MQTT over TLS. A **presence message** of the following form  
has to be send to the service in order to establish a connection to a MQTT broker:

    {  
        "name":"unsecureMqttBroker",
        "brokerName":"{base topic}",
        "ip":"{host and port}"
    }

This will instantiate a new messaging channel to the given MQTT broker and all messages to send have to be prefixed with the 
**{base topic}** in order to be send to that broker. The same scheme applies to subscriptions and the topic for received messages.

It is possible to create a connection to a secure MQTT broker over TLS:

    {  
        "name":"secureMqttBroker",
        "brokerName":"{base topic}",
        "ip":"{host and port}",
        "cafile":"{certificate authority certificate file location}",
        "certfile":"{client certificate file location}",
        "certpassword":"{client certificate password}",
        "keyfile":"{client key file}"
    }

This will spawn a new MQTT client instance to the MQTT broker secured by TLS and it uses the given certificates in order to establish the
secure connection.

### ch.hevs.inuit.components
This bundle contains several useful components. These components can be instantiated by sending **appearance messages**.

#### Boolean to Event
Listens to a boolean value and sends events when the value changes.

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "boolean2Event",
        "booleanTopic": "{input topic}",
        "eventTopic": "{output topic}"
    }

#### Event Counter
Listens to two topics, if an event is received in the first one, the value is incremented, if an event is received on the second topic,
the counter will be decremented.

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "eventCounter",
        "incTopic": "{increment topic}",
        "decTopic": "{decrement topic}",
        "counterTopic": "{output topic}"
    }

#### Internal Event Integrator
Allows to integrate a value on each time an event from the given **internal** event source has been received.

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "internalEventIntegrator",
        "interruptName": "gpioInterrupt7",
        "integrationPerTick": "{amount to increment per event}",
        "topic": "{output topic}"
    }

#### Messaging send
Allows anyone to send a MQTT message to the given topic.

The **appearance message** in order to send a message looks like this:

    {
        "name": "messagingSend",
        "topic": "{message topic}",
        "payload": "{message payload}",
        "qos": "{quality of service (check MQTT spec for details)}",
        "retain": "{retain message (true/false)}"
    }
    
**qos** and **retain** are per default 0 and false, so most of the time this two fields can be omitted.

#### Message Redirection
Redirects a message received in one topic to another topic.

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "messagingRedirect",
        "from": "{input topic}",
        "to": "{output topic}"
    }

#### Periodic Message Sender
Sends the given message periodically to the given topic. We agree that it's use is quite limited...

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "periodicMessage",
        "topic": "{output topic}",
        "msg": "{message to send}",
        "period": {period in seconds (floating point)}
    }

#### Time Counter

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "timeCounter",
        "startTopic": "{a message to this topic starts the timer}",
        "stopTopic": "{a message to this topic stops the timer}",
        "resetTopic": "{a message to this topic resets the timer}",
        "timeTopic": "{topic where the final time is send to}",
        "tickTopic":" "{ticks are send to this topic during timer run}",
        "tickPeriodeMs": {tick period in milliseconds},
        "mode": {Model to send at instantiation}
    }

#### Wave Player
This component plays a WAV file upon reception of a message on the given topic.

The **appearance message** in order to instantiate the component looks like this:

    {
        "name": "wavePlayer",
        "topic": "{input topic}",
        "sound": "{path to sound file to play}"
    }

### ch.hevs.inuit.nodered
This bundle contains the configuration, additional nodes for Node-Red (inuit category) and can be used to start Node-Red by sending the 
following **appearance message**:

    {
        "name": "nodered"
    }

Node-RED is a tool for wiring together hardware devices, APIs and online services in new and interesting way. Node-RED doesn't take away 
the need to write code altogether, but it does reduce it, and in the process both lowers the technical bar and allows people to focus on 
the creating, rather than on the doing. Just as a spreadsheet lets you to play around with numbers, Node-RED is a tool that's good for 
playing around with events and dispatch them.

### ch.hevs.inuit.cloudpush
This service pushes sensor data to the iNUIT sensor cloud using RESTful web services.

In order to create a service instance connected to a given iNUIT cloud server, send the following appearance message:

    {  
        "name":"cloudPusher",
        "baseUrl":"{host and port}",
        "topic":"{config topic}"
    }

This will create an instance of the service that will use the given base URL in order to push the data to the cloud RESTful web service.
The service will listen to the given topic and upon receiving a special message it will add a subscription to a sensor and publish the 
sensor's data to the iNUIT cloud. The **MQTT message** to send looks like the following:

*topic*: {config topic}/{sensor base topic}
*content*: {sensor ID}

This means that the topic to send the message to enable the data pushing to the cloud of a given sensor is the base config topic of the 
cloud push service plus the base topic of the sensor (A sensor should always use 2 topics: {sensor base}/model in order to publish his 
data model and {sensor base}/data in order to push samples). The content of the message has to be the **iNUIT cloud UUID** of the sensor.

The JSON schemes for the sensor model and sensor data can be found [here](http://inuit.iict.ch/1/spec)

An Example:

    {  
        "name":"cloudPusher",
        "baseUrl":"http://inuit.iict.ch",
        "topic":"default/cloud"
    }

If we want now push the data of the sensor with the root topic **/default/testsensor** and the UUID 
**550e8400-e29b-41d4-a716-446655440000** to the cloud, we have to send a message with the following content:

    550e8400-e29b-41d4-a716-446655440000

to the topic:

    default/cloud/default/testsensor

and the cloud push service will push all messages send to the topics **/default/testsensor/model** and **/default/testsensor/data** 
to the cloud RESTful API by adding the sensor ID to the JSON payload but without modifying the actual data or format of the JSON message.
In other words, the sensors on the gateway have to use the very same message format as it is used by the iNUIT cloud API.

#### Local REST API facade
This is a simple python script that aims to reproduce the iNUIT cloud API locally. Note that the API is not complete and less strict than
the original API, but it was handy in order to complete the first tests. Note that the scripts depends on [bottle.py](http://bottlepy.org).

### ch.hevs.inuit.mongopusher
This service pushes sensor data to a mongo DB database.

In order to create a service instance connected to a given mongoDB server, send the following appearance message:

    {  
        "name":"mongoPusher",
        "ip":"{host}",
        "database": "{database name}",
        "topic":"{config topic}"
    }

This will create an instance of the service that will use the given mongoDB document base on the given mogno DB server.
The service will listen to the given topic and upon receiving a special message it will add a subscription to a sensor and save the 
sensor's data to the mongoDB database. The **MQTT message** to send looks like the following:

*topic*: {config topic}/{sensor base topic}
*content*: {"id": "ID String"}

This means that the topic to send the message to enable the data pushing to the database of a given sensor is the base config topic of the 
mongo push service appended with the base topic of the sensor (A sensor should always use 2 topics: {sensor base}/model in order to publish his 
data model and {sensor base}/data in order to push samples).

If we want now push the data of the sensor with the root topic **/default/testsensor** and the UUID 
**550e8400-e29b-41d4-a716-446655440000** to the cloud, we have to send a message with the following content:

    {
        "id": "550e8400-e29b-41d4-a716-446655440000" 
    }

to the topic:

    default/mongo/default/testsensor

if the mongo push topic registration topic would have been **default/mongo**. The mongo push service will save all messages send to the 
topics **/default/testsensor/model** and **/default/testsensor/data** to the configured mongo DB.

### ch.hevs.inuit.graphitepusher
This service pushes sensor data to a graphite time series database.

In order to create a service instance connected to a given graphite server, send the following appearance message:

    {  
        "name":"graphitePusher",
        "ip":"{host}",
        "port": "{port}"
        "topic":"{config topic}"
    }

This will create an instance of the service that will send the data on subscribed topics to the given graphite server. The service will 
listen to the given topic and upon receiving a special message it will add a subscription to a sensor and send the sensor's timestamped
data to the graphite database. The **MQTT message** to send looks like the following:

*topic*: {config topic}/{sensor data topic}
*content*: *no content*

This means that the topic to send the message to enable the data pushing to the graphite server of a given sensor is the base config topic 
of the graphite push service appended with the data topic of the sensor (A sensor should always use 2 topics: {sensor base}/model in order 
to publish his data model and {sensor base}/data in order to push samples). Note that the graphite service ignores the /model part and you
need to send the complete topic including the /data token.

If we want now push the data of the sensor with the root topic **/default/testsensor**, we have to send an empty message to the topic:

    default/graphite/default/testsensor/data

if the mongo push topic registration topic would have been **default/graphite**. The graphite push service will save all messages send to 
the topic **/default/testsensor/data**to the configured graphite timeseries DB.

### ch.hevs.inuit.driver.raspberrypi
Disposes the drivers used to make the link between MQTT and the peripherals on the raspberry PI. The following chapters will describe the 
available peripherals.

#### I2CBus
Implements the I2C service for the raspberry pi platform. 

The **appearance message** in order to instantiate a I2C instance looks like this:

    {  
      "name":"piI2cBus",
      "busId": {bus id}
    }
    
This will create the I2C implementation for the raspberry ip and register the instance at **piI2CBus{number}** on the global directory 
service.

#### GpioInterrupt
Allows to use a Raspberry PI GPIO interrupt as event source.

The **appearance message** in order to instantiate a GPIO interrupt service instance looks like this:

    {  
      "name":"piGpioInterrupt",
      "pinId":{pin number}
    }

The numbering of the pins follows the [WiringPi](http://wiringpi.com) library.

The event source is registered on the iNUIT global directory under the name **gpiioInterrupt{pin number}**. Note that you need to connect
a component that reacts on these events to actually send data via MQTT.

#### GpioOutput
Configures the given GPIO pin as output. The output can be controlled by writing to the topic given to the **appearance message**:

    {  
      "name":"piGpioOutput",
      "pinId":{pin number},
      "topic":"{control topic}"
    }
    
The message send to the output should conform the iNUIT json scheme and containing at least one boolean sample value. Note that in the case
the message carries multiple values, the last one will be considered.

#### GpioInput
Configures the given GPIO pin as input. The input will send JSON sample batch messages for each change to the topic given to the 
**appearance message**:

    {  
      "name":"piGpioInput",
      "pinId":{pin number},
      "topic":"{publish topic}"
    }

The message send from the input conforms the iNUIT json scheme and contains the actual value and timestamp.

#### PiFace
Driver for the PiFace digital I/O card for the RaspberryPi. The PiFace is an extension consisting of 4 buttons, 8 digital logic level output 
and 2 relay contacts.

The **appearance message** in order to instantiate the PiFace driver looks like this:

    {  
      "name":"piFace",
      "topic":"{base topic}"
    }
    
The PiFace will send events every times a button changes to the topic **{base topic}/button/{number}/data**, whereas *{number}* identifies
the actual button which has changed (0...3). The message format is a SampleBatch with boolean values.

The outputs can be controlled by writing to the given topics:

**{base topic}/led/{number}/data**  
This changes the digital output with the given number to the actual value inside the boolean sample batch. *{number}* can be from 0 to 7.

**{base topic}/relay/{number}/data**  
This changes the relay contact output with the given number (0 or 1) to the actual value inside the boolean sample batch. Note that the 
relay output 0 is shared with the digital output 0 and relay output 1 with digital output 1, so if you change one of them, it will effectively
affect both. 

### ch.hevs.inuit.sensor.adt7240
This drivers allows to send MQTT messages containing the actual temperature measured by the I2C temperature sensor **ADT7240** connected to an
I2C bus. Note that the driver will use the iNUIT I2C interface in order to communicate with the sensor. So the driver does not depend on any 
particular platform.

To create an instance of the sensor driver, send the following **appearance message**:

    {  
      "name":"ADT7240",
      "busName":"{I2C bus to use}",
      "address":{I2C Address of the sensor},
      "topic":"{topic to send data to}",
      "period":{sample period in seconds},
    
      "model":{data model to send}
    }
    
It will use the I2C interface provided by *{I2C bus to use}* in order to read the temperature and send messages all *period* seconds.

### ch.hevs.inuit.sensor.ad7991
This drivers allows to send MQTT messages containing the actual analogue input value measured by the I2C 4 channel ADC **AD7991** 
connected to an I2C bus. Note that the driver will use the iNUIT I2C interface in order to communicate with the sensor. So the driver 
does not depend on any particular platform.

To create an instance of the sensor driver, send the following **appearance message**:

    {  
      "name":"AD7991",
      "busName":"{I2C bus to use}",
      "address":{I2C Address of the ADC},
      "period":{sample period in seconds},
    
      "channels":[  
        {  
          "topic":"{channel 0 topic}",
          "offset":{offset},
          "factor":{multiplication factor},
          "model":{data model to send}
        },
        {  
          "topic":"{channel 1 topic}"
        },
        {  
          "topic":"{channel 2 topic}"
        },
        {  
          "topic":"{channel 3 topic}"
        }
        
      ]
    }

It will use the I2C interface provided by *{I2C bus to use}* in order to read the analogue values and send messages all *period* seconds.
Note that you can define up to 4 channels that are send by the ADC driver and for each channel you have to provide the mandatory property 
**topic**. You can optionally provide a data model and configure offsets and factors for each channel (shown for channel 0). 

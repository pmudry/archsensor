package ch.hevs.inuit.directory.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import ch.hevs.inuit.directory.Directory;

import ch.hevs.inuit.library.service.IDirectory;

public class Activator implements BundleActivator {
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	Directory directory;

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;

		directory = new Directory();
		bundleContext.registerService(IDirectory.class.getName(), directory, null);
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}

package ch.hevs.inuit.directory;

import java.util.HashMap;

import ch.hevs.inuit.library.service.IDirectory;

public class Directory implements IDirectory{
	HashMap<Object, Object> map = new HashMap<Object, Object>();
	@Override
	public void put(Object key, Object value) {
		map.put(key, value);
	}

	@Override
	public void remove(Object key) {
		map.remove(key);
	}

	@Override
	public Object get(Object key) {
		return map.get(key);
	}
	
}

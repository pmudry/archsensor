package ch.hevs.inuit.library.json.model;

import ch.hevs.inuit.library.misc.FillableHelper;


public class Location extends FillableHelper{	
	public Double lat,lon,alt;
	public String name;
	
	public Location setAlt(Double alt) {
		this.alt = alt;
		return this;
	}
	public Location setLon(Double lon) {
		this.lon = lon;
		return this;
	}
	public Location setLat(Double lat) {
		this.lat = lat;
		return this;
	}
	public Location setName(String name) {
		this.name = name;
		return this;
	}
}
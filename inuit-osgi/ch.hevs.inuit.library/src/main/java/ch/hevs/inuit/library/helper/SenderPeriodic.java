package ch.hevs.inuit.library.helper;

import java.util.Timer;
import java.util.TimerTask;

import ch.hevs.inuit.library.service.IMessagingService;

/**
 * Send each given time a object on specified topic by using messaging service
 */
public abstract class SenderPeriodic {
	Timer timer;
	String topic;
	IMessagingService messaging;
	
	public SenderPeriodic(long msPeriod,String topic,IMessagingService messaging) {
		this.messaging = messaging;
		this.topic = topic;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				getObjectToSend();
			}
		}, msPeriod, msPeriod);
		
		
	}
	protected abstract Object getObjectToSend();
	
	public String getTopic() {
		return topic;
	}
}

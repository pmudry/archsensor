package ch.hevs.inuit.library.misc;

public interface IDestructable {
	void destruct();
}

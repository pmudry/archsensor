package ch.hevs.inuit.library.helper;

import ch.hevs.inuit.library.json.NumberBatch;
import ch.hevs.inuit.library.misc.IFloatSource;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.Message;

/**
 * Same than SenderPeriodic but send a batch<float> filled with a FloatSource.
 */
public class SenderPeriodicFloat extends SenderPeriodic{
	IFloatSource floatSource;
	public SenderPeriodicFloat(IFloatSource floatSource,long msPeriod, String topic,IMessagingService messaging) {
		super(msPeriod, topic,messaging);
		this.floatSource = floatSource;
	}

	@Override
	protected Object getObjectToSend() {
		NumberBatch sample = new NumberBatch();
		sample.setValue((double)floatSource.getFloatValue());
		sample.updateTimeStamp();
		messaging.send(new Message(sample), getTopic());
		return sample;
	}
}

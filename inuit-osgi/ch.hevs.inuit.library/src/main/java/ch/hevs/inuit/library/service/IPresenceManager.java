package ch.hevs.inuit.library.service;


/**
 * iNUIT osgi service, that could be used for example to notify drivers that a device was plugged at a given location
 */
public interface IPresenceManager {
	void subscribe(IPresenceObserver observer);
	void unsubscribe(IPresenceObserver observer);
	
	void notifyAppear(Object o);
	void notifyDisappear(Object o);
}

package ch.hevs.inuit.library.event;

public interface IEventSource {
	void subscribe(IEventSink sink);
	void unsubscribe(IEventSink sink);
}

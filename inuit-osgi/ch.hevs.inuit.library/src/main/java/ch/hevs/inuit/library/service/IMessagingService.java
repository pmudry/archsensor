package ch.hevs.inuit.library.service;


/**
 * iNUIT osgi messaging service used to tx/rx message over a network
 * In fact, this network is constituted by some MQTT broker
 */

public interface IMessagingService {
	void subscribe(IMessagingSubscriber subscriber,String topic);
	void unsubscribe(IMessagingSubscriber subscriber,String topic);
	
	void send(Message message,String topic);
}

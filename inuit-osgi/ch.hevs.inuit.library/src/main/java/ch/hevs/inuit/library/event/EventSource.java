package ch.hevs.inuit.library.event;

import java.util.HashSet;

/**
 * Basic implementation of a event source with subscription management (Broadcast)
 */
public class EventSource implements IEventSource{
	HashSet<IEventSink> sinks = new HashSet<IEventSink>();
	
	@Override
	public void subscribe(IEventSink sink) {
		sinks.add(sink);
	}

	@Override
	public void unsubscribe(IEventSink sink) {
		sinks.remove(sink);
	}
	
	/**
	 * Broadcast notification
	 */
	protected void pushEvent(){
		for(IEventSink s : sinks){
			s.eventFrom(this);
		}
	}

}

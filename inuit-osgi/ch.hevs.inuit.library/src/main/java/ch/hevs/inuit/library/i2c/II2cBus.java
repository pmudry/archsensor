package ch.hevs.inuit.library.i2c;

import java.io.IOException;

public interface II2cBus {
	/**
	 * 
	 * @param address I2C address of the device
	 * @return
	 * @throws IOException
	 */
	II2cDevice getDevice(int address) throws IOException;
}

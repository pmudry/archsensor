package ch.hevs.inuit.library.json;

import ch.hevs.inuit.library.service.Message;


public class BooleanBatch extends SampleBatch<Boolean>{
	public BooleanBatch() {
		
	}

	public static BooleanBatch from(Message message) {
		return Json.to(message, BooleanBatch.class);
	}
}

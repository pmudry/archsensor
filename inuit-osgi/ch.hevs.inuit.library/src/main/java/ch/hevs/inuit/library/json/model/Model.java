package ch.hevs.inuit.library.json.model;

import java.util.UUID;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.json.model.Refresh.Periodic;
import ch.hevs.inuit.library.misc.FillableHelper;

/**
 * This class is the java version of the json model that must be send to the cloud
 * to describe each sensor
 */
public class Model extends FillableHelper{
	public UUID id, group;
	public String name;
	public String description;

	public Sampling sampling;
	public Hardware hardware;
	public Software software;
	public Location location;

	public Model setName(String name) {
		this.name = name;
		return this;
	}

	public Model setDescription(String description) {
		this.description = description;
		return this;
	}


	public static void main(String[] args) {
		Model m = new Model();
		m.id = UUID.randomUUID();
		m.name = "Z";
		m.sampling = new Sampling();
		m.sampling.refresh = new Periodic().setPeriod(0.1f);



		String json = Json.from(m);
		System.out.println(json);
		json = json.replace("type", "caca");
		Json.to(json, Model.class);
	}

}

/*
 * public Model setId(UUID id) { this.id = id; return this; }
 * 
 * public Model setGroup(UUID group) { this.group = group; return this; }
 * 
 * public Model setName(String name) { this.name = name; return this; }
 * 
 * public Model setDescription(String description) { this.description =
 * description; return this; }
 * 
 * public Sampling getSampling() { return sampling; }
 */

/*
 * public Sampling setPeriod(Float period) { this.period = period; return this;
 * }
 * 
 * public Sampling setAccuracy(Float accuracy) { this.accuracy = accuracy;
 * return this; }
 */

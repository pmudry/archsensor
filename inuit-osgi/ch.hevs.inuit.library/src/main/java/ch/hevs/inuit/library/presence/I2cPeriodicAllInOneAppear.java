package ch.hevs.inuit.library.presence;

import ch.hevs.inuit.library.json.model.Model;

public class I2cPeriodicAllInOneAppear extends I2cDeviceAppear{
	public Float period;
	public String topic;
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && period != null && topic != null;
	}
}

package ch.hevs.inuit.library.json.model;

import ch.hevs.inuit.library.misc.FillableHelper;

public class Range extends FillableHelper{
	public Double from,to;
	
	public Range set(Double from, Double to){
		this.from = from;
		this.to = to;
		return this;
	}
}

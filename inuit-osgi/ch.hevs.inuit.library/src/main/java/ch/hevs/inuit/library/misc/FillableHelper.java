package ch.hevs.inuit.library.misc;

/**
 * Base implementation of IFillable for lazy people like me
 */
public class FillableHelper implements IFillable{
	@Override
	public void fillWith(Object o) {
		Utils.fill(this, o);
	}
}

package ch.hevs.inuit.library.json.model;

import java.lang.reflect.Type;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.misc.FillableHelper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


public class Physical  extends FillableHelper{
	public enum PhysicalType {
		temperature, humidity, atmosphericPressure, brightness,none
	};
	
	public PhysicalType type;
	
	

	
	
	public static class RangedPhysical extends Physical{		
		public Range range;
	}	
	
	public static class None extends RangedPhysical{
		public None() {
			type = PhysicalType.none;
		}
	}
	
	public static class Temperature extends RangedPhysical{
		public Temperature() {
			type = PhysicalType.temperature;
		}
		
		public Temperature(Double from,Double to){
			this();
			range = new Range();
			range.set(from,to);
		}
	}
	
	
	
	public static class JsonSerDes implements JsonDeserializer<Physical>, JsonSerializer<Physical>
	{
		@Override
		public JsonElement serialize(Physical json, Type typeOfT, JsonSerializationContext ctx) {
			switch (json.type) {
			case temperature:
				return Json.gson.toJsonTree(json, Temperature.class);
			case none:
				return Json.gson.toJsonTree(json, None.class);
			default:
				break;
			}
			return Json.gson.toJsonTree(json, Physical.class);
			
		}
		
	    @Override
	    public Physical deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) throws JsonParseException
	    {
	        JsonObject obj = json.getAsJsonObject();
	        JsonElement jsonType = obj.get("type");
	        if(jsonType == null) return null;
	        String stringType = jsonType.getAsString();
	        PhysicalType type = null;
	        try {
		        type = PhysicalType.valueOf(stringType);	        				
			} catch (Exception e) {
				return null;
			}

	        switch (type) {
	        case temperature:
	        	return Json.to(json, Temperature.class);
	        case none:
	        	return Json.to(json, None.class);
			default:
				break;
			}
	        return Json.to(json, Physical.class);
	    }
	}
	
}
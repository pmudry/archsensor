package ch.hevs.inuit.library.event;

public interface IEventSink {
	void eventFrom(IEventSource source);
}

package ch.hevs.inuit.library.json.model;

import ch.hevs.inuit.library.misc.FillableHelper;


public class Hardware extends FillableHelper{	
	public String manufacturer ,model;
	
	public Hardware setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
		return this;
	}
	public Hardware setModel(String model) {
		this.model = model;
		return this;
	}
}
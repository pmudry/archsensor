package ch.hevs.inuit.library.json.model;

import java.lang.reflect.Type;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.misc.FillableHelper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class Refresh extends FillableHelper {
	public enum RefreshType {
		periodic, event, pulling
	};

	RefreshType type;

	public static class Periodic extends Refresh {
		public Periodic() {
			type = RefreshType.periodic;
		}

		public Float period;

		public Periodic setPeriod(double period) {
			this.period = (float) period;
			return this;
		}
	}

	public static class JsonSerDes implements JsonDeserializer<Refresh>, JsonSerializer<Refresh>
	{
		@Override
		public JsonElement serialize(Refresh json, Type typeOfT, JsonSerializationContext ctx) {
			switch (json.type) {
			case periodic:
				return Json.gson.toJsonTree(json, Periodic.class);
			default:
				break;
			}
			return Json.gson.toJsonTree(json, Refresh.class);

		}

		@Override
		public Refresh deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) throws JsonParseException
		{

			JsonObject obj = json.getAsJsonObject();
			JsonElement jsonType = obj.get("type");
			if (jsonType == null)
				return null;
			String stringType = jsonType.getAsString();
			RefreshType type = null;
			try {
				type = RefreshType.valueOf(stringType);
			} catch (Exception e) {
				return null;
			}

			switch (type) {
			case periodic:
				return Json.to(json, Periodic.class);
			default:
				break;
			}
			return Json.to(json, Refresh.class);
		}
	}

}
package ch.hevs.inuit.library.service;

/**
 * iNUIT osgi service that consist to have a hashmap 
 * to share for example a I2CBus with a given key (could be a string)
 */
public interface IDirectory {
	void put(Object key,Object value);
	void remove(Object key);
	
	Object get(Object key);
}

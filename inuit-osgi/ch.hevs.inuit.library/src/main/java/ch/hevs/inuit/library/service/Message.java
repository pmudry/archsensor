package ch.hevs.inuit.library.service;

import java.io.UnsupportedEncodingException;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.json.model.Model;

/**
 * Message that could be tx/rx with IMessagingService
 */
public class Message {
	byte[] payload = null;
	int qos = 0; //0 => fire and forget   1 => everybody must receive one or more time    2 => everybody must receive one time
	boolean retained = false; //Message must additionally be stored into the broker and published again at each subscription 
							  //Only one message can be retain per topic
	
	
	
	public Message() {
		
	}
	
	public Message(Model m) {
		setRetained(true);
		setQos(1);
		setPayloadObject(m);
	}
	
	public Message(Object o) {
		setPayloadObject(o);
	}
	public Message(String payload) {
		setPayload(payload);
	}
	public Message setPayload(byte[] payload) {
		this.payload = payload;
		return this;
	}
	
	public Message setPayload(String payload) {
		this.payload = payload.getBytes();
		return this;
	}	
	/**
	 * Fill the message payload with json transformation of the given object
	 */
	public Message setPayloadObject(Object o) {
		this.payload = Json.from(o).getBytes();
		return this;
	}
	
	
	public Message setQos(int qos) {
		this.qos = qos;
		return this;
	}
	public Message setRetained(boolean retained) {
		this.retained = retained;
		return this;
	}
	
	
	public String getPayloadString(){
		try {
			return new String(getPayload(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	public byte[] getPayload() {
		return payload;
	}
	public int getQos() {
		return qos;
	}
	public boolean isRetained(){
		return retained;
	}
	
	/**
	 * A setup that could be used if you want to transmit a state at each change (ex switch)
	 */
	public Message setAsEventStateQos() {
		return this.setRetained(true).setQos(1);
	}
}

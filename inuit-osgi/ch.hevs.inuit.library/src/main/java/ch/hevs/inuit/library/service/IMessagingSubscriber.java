package ch.hevs.inuit.library.service;

/**
 * How to subscribe to message on IMessagingService
 */
public interface IMessagingSubscriber {
	void receive(String topic, Message message);
}

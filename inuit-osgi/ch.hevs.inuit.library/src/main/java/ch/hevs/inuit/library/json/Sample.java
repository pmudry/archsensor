package ch.hevs.inuit.library.json;

import java.util.UUID;

public class Sample<DataType> {
	public Sample() {
		
	}

	public Sample<DataType>  setId(UUID id) {
		this.id = id;
		return this;
	}
	
	
	public Sample<DataType>  setValue(DataType v) {
		this.v = v;
		return this;
	}
	
	public Sample<DataType> updateTimeStamp(){
		t = System.currentTimeMillis();
		return this;
	}
	
	UUID id;
	Long t;
	
	DataType v;


}

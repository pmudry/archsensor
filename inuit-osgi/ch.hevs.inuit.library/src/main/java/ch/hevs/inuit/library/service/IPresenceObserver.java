package ch.hevs.inuit.library.service;


/**
 * How to be notified from presenceManger
 */
public interface IPresenceObserver {
	boolean appear(Object o);
	boolean disappear(Object o);
}

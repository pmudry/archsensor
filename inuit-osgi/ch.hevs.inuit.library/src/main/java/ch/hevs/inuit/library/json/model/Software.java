package ch.hevs.inuit.library.json.model;

import org.osgi.framework.BundleContext;

import ch.hevs.inuit.library.misc.FillableHelper;


public class Software extends FillableHelper{	
	
	public SoftwareItem os;
	public SoftwareItem app;
	/*public String os ,osVer;
	public String app ,appVer;
	
	
	public Software setApp(String app) {
		this.app = app;
		return this;
	}
	public Software setAppVer(String appVer) {
		this.appVer = appVer;
		return this;
	}
	
	public Software setOs(String os) {
		this.os = os;
		return this;
	}
	
	public Software setOsVer(String osVer) {
		this.osVer = osVer;
		return this;
	}*/
	
	
	static class SoftwareItem extends FillableHelper{
		public SoftwareItem() {
		}
		
		public SoftwareItem(String name,String version) {
			this.name = name;
			this.version = version;
		}
		
		String name;
		String version;
	}
	
	
	public static Software from(BundleContext context){
		Software s = new Software();
		
		
		s.os = new SoftwareItem(  System.getProperty("os.name"),
								  System.getProperty("os.version"));
		/*s.app = new SoftwareItem( context.getBundle().getSymbolicName(),
								  context.getBundle().getVersion().toString());*/
		
	/*	s.setApp(context.getBundle().getSymbolicName())
		 .setAppVer(context.getBundle().getVersion().toString())
		 .setOs(System.getProperty("os.name"))
		 .setOsVer(System.getProperty("os.version"));*/
		 
		return s;
	}
}	

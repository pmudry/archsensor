package ch.hevs.inuit.library.i2c;

import java.io.IOException;

public interface II2cDevice {
	void write(int address, byte[] buffer, int offset, int size) throws IOException;
	void write(byte[] buf, int offset, int size) throws IOException;
	int read(int address, byte[] buffer, int offset, int size) throws IOException;
	int read(byte[] buffer, int offset, int size) throws IOException;
	
}

package ch.hevs.inuit.library.misc;

import java.lang.reflect.Field;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.presence.NamedPresence;

public class Utils {
	
	
	/**
	 * Fill the dst object with src object.
	 * That use reflexion. If a attribut implement IFillable => that call fillWith for this attribut.
	 */
	public static void fill(Object dst, Object src) {
		if (src == null || dst == null)
			return;
		//Field[] f = dst.getClass().getFields();
		for (Field fDst : dst.getClass().getFields()) {
			Field fSrc;
			Object oDst = null, oSrc = null;
			try {
				fSrc = src.getClass().getField(fDst.getName());
				oDst = fDst.get(dst);
				oSrc = fSrc.get(src);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				continue;
			}
			
			if(oSrc == null) continue;
 			if(fDst.getType().isAssignableFrom(oSrc.getClass()) == false)
				continue;

			if (oDst == null) {
				if (oSrc != null) {
					try {
						fDst.set(dst, oSrc);
					} catch (IllegalArgumentException | IllegalAccessException e) {

					}
				}
			} else if (oDst instanceof IFillable) {
				try {
					fill(fDst.get(dst), fSrc.get(src));
				} catch (IllegalArgumentException | IllegalAccessException e) {

				}
			} else {
				try {
					if (oSrc != null) {
						fDst.set(dst, oSrc);
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					continue;
				}
			}

		}
	}
	
	
	
	
	/**
	 * Usefull function to check if a presence object can be converted into the specified NamedPresence class.
	 * Conversion could be done from json string or from instance
	 * @param data the data to translate into c type
	 * @param name The NamedPresence translated must have this name
	 * @param c The returned class type
	 * @return Instance of c with data filled, else null
	 */
	public static <T extends NamedPresence> T getAppear(Object data,String name,Class<T> c){
		
		//From instance
		if(c.isAssignableFrom(data.getClass())){		
			@SuppressWarnings("unchecked")
			T presence = (T)data;
			if(presence.check() && presence.name.equals(name)) 
				return presence;
			else
				return null;
		}
		
		//From String
		if(data instanceof String == false) return null;
		String str = (String)data;
		if(str.contains(name) == false) return null;
		T appear = Json.to((String)data, c);
		if(appear.check() && appear.name.equals(name))
			return appear;

		return null;
	}	
}

package ch.hevs.inuit.library.presence;

public class MessagingPresence extends NamedPresence{
	public String topic;
	@Override
	public boolean check() {
		return super.check() && topic != null;
	}
}

package ch.hevs.inuit.library.json.model;

import java.lang.reflect.Type;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.misc.FillableHelper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


public class Accuracy extends FillableHelper{
	public enum AccuracyType {
		absolute
	};

	AccuracyType type;
	
	Double resolution;
	
	public Accuracy setResolution(Double resolution) {
		this.resolution = resolution;
		return this;
	}
	
	public static class Absolute extends Accuracy {
		public Absolute() {
			type = AccuracyType.absolute;
		}

		public Absolute(Double absolute,Double resolution) {
			this();
			this.resolution = resolution;
			this.absolute =  absolute;
		}

		Double absolute;

		public Absolute setAbsolute(Double absolute) {
			this.absolute = absolute;
			return this;
		}
	}
	
	
	
	
	public static class JsonSerDes implements JsonDeserializer<Accuracy>, JsonSerializer<Accuracy>
	{
		@Override
		public JsonElement serialize(Accuracy json, Type typeOfT, JsonSerializationContext ctx) {
			switch (json.type) {
			case absolute:
				return Json.gson.toJsonTree(json, Absolute.class);
			}
			return Json.gson.toJsonTree(json, Accuracy.class);
			
		}
		
	    @Override
	    public Accuracy deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) throws JsonParseException
	    {

	        JsonObject obj = json.getAsJsonObject();
	        JsonElement jsonType = obj.get("type");
	        if(jsonType == null) return null;
	        String stringType = jsonType.getAsString();
	        AccuracyType type = null;
	        try {
		        type = AccuracyType.valueOf(stringType);	        				
			} catch (Exception e) {
				return null;
			}

	        switch (type) {
	        case absolute:
	        	return Json.to(json, Absolute.class);
			}
	        return Json.to(json, Accuracy.class);
	    }
	}
	
	

}
package ch.hevs.inuit.library.helper;

import java.net.URL;
import java.util.HashSet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import ch.hevs.inuit.library.misc.IDestructable;
import ch.hevs.inuit.library.misc.Utils;
import ch.hevs.inuit.library.presence.NamedPresence;
import ch.hevs.inuit.library.service.IDirectory;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IPresenceManager;
import ch.hevs.inuit.library.service.IPresenceObserver;


/**
 * Base implementation of a inuit device driver plugin
 * - Contain reference of all commonly used plugin
 * - Appear and disappear from presenceManager are redirected to class kind class
 * - osgiAdt7240 is a easy to understood example
 */
public abstract class DeviceActivatorHelper implements BundleActivator {
	private BundleContext context;

	protected BundleContext getContext() {
		return context;
	} 
	
	//Commonly used service
	protected IPresenceManager presenceManager;
	protected IDirectory directory;
	protected IMessagingService messagingService;


	private HashSet<Object> kinds = new HashSet<Object>();

	//When this bundle create a instance of, for example, device, you must register it into kinds
	//When the bundle is unloaded, all kinds that implement IDestructable are destroyed
	protected void addKind(Object o){
		kinds.add(o);
	}
	
	//start bundle
	public void start(BundleContext bundleContext) throws Exception {
		context = bundleContext;

		ServiceReference presenceManagerReference = bundleContext.getServiceReference(IPresenceManager.class.getName());
		presenceManager = (IPresenceManager) bundleContext.getService(presenceManagerReference);
		
		ServiceReference directoryReference = bundleContext.getServiceReference(IDirectory.class.getName());
		directory = (IDirectory) bundleContext.getService(directoryReference);

		ServiceReference mqttServiceReference = bundleContext.getServiceReference(IMessagingService.class.getName());
		messagingService = (IMessagingService) bundleContext.getService(mqttServiceReference);

		//notify kind class of presence manager events
		presenceManager.subscribe(new IPresenceObserver() {
			@Override
			public boolean disappear(Object o) {
				return DeviceActivatorHelper.this.disappear(o);
			}
			
			@Override
			public boolean appear(Object o) {
				return DeviceActivatorHelper.this.appear(o);
			}
		});
		

	}


	//Stop bundle
	public void stop(BundleContext bundleContext) throws Exception {
		
		//stop kinds
		for(Object o : kinds){
			if(o instanceof IDestructable){
				((IDestructable)o).destruct();
			}
		}
		kinds.clear();
		context = null;
	}
	
	/**
	 * Usefull function to check if a presence object can be converted into the specified NamedPresence class.
	 * Conversion could be done from json string or from instance
	 * @param data the data to translate into c type
	 * @param name The NamedPresence translated must have this name
	 * @param c The returned class type
	 * @return Instance of c with data filled, else null
	 */
	protected <T extends NamedPresence> T getAppear(Object data,String name,Class<T> c){
		return Utils.getAppear(data,name,c);
	}	
	
	//Presence manager notifications
	protected abstract boolean appear(Object o);
	protected abstract boolean disappear(Object o);


}

package ch.hevs.inuit.library.json;

import java.lang.reflect.Array;
import java.util.UUID;

import ch.hevs.inuit.library.service.Message;

class SampleBatch<DataType> {
	
	public SampleBatch() {
		
	}

	public SampleBatch(Message message) {
		Json.to(message.getPayloadString(), this.getClass());
	}

	public SampleBatch<DataType>  setId(UUID id) {
		this.id = id;
		return this;
	}
	
	
	public SampleBatch<DataType>  setValue(DataType[] v) {
		this.v = v;
		return this;
	}
	
	public DataType getLastValue() {
		return v[v.length-1];
	}
	

	@SuppressWarnings("unchecked")
	public SampleBatch<DataType> setValue(DataType f) {
		v = (DataType[]) Array.newInstance(f.getClass(), 1);
		v[0] = f;
		return this;
	}
	
	public SampleBatch<DataType> updateTimeStamp(){
		t = new Long[v.length];
		for(int idx = 0;idx < t.length;idx++){
			t[idx] = System.currentTimeMillis();
		}
		return this;
	}
	
	public UUID id;
	public Long[] t;
	
	public DataType[] v;
}

package ch.hevs.inuit.library.json.model;

import ch.hevs.inuit.library.misc.FillableHelper;

public class Sampling extends FillableHelper {
	public Physical physical;
	public Refresh refresh;
	public Accuracy accuracy;
	
	public Sampling setPhysical(Physical physical) {
		this.physical = physical;
		return this;
	}

	public Sampling setRefresh(Refresh refresh) {
		this.refresh = refresh;
		return this;
	}

	public Sampling setAccuracy(Accuracy accuracy) {
		this.accuracy = accuracy;
		return this;
	}
}

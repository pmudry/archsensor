package ch.hevs.inuit.library.misc;

public interface IFloatSource {
	float getFloatValue();
}

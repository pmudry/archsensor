package ch.hevs.inuit.library;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		
		
		/*String str = "{\"v\":[false]}";
		BooleanBatch sample = Json.to(str, BooleanBatch.class);
		sample.v = new Boolean[]{(Boolean)true,(Boolean)false};
		String ttr = Json.from(sample);
		*/
		Activator.context = bundleContext;

	}

	public void stop(BundleContext bundleContext) throws Exception {

		Activator.context = null;
	}

}

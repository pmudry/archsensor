package ch.hevs.inuit.library.json;

import ch.hevs.inuit.library.json.model.Accuracy;
import ch.hevs.inuit.library.json.model.Physical;
import ch.hevs.inuit.library.json.model.Refresh;
import ch.hevs.inuit.library.service.Message;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * This class contain a Gson instance with all type adapter that iNUIT need.
 * (JsonSerDes)
 */
public class Json {

	public static Gson gson;
	public static Gson gsonWellPrinted;
	static {
		gson = new GsonBuilder()
				.registerTypeAdapter(Refresh.class, new Refresh.JsonSerDes())
				.registerTypeAdapter(Accuracy.class, new Accuracy.JsonSerDes())
				.registerTypeAdapter(Physical.class, new Physical.JsonSerDes())
				.create();
		gsonWellPrinted = new GsonBuilder()
				.registerTypeAdapter(Refresh.class, new Refresh.JsonSerDes())
				.registerTypeAdapter(Accuracy.class, new Accuracy.JsonSerDes())
				.registerTypeAdapter(Physical.class, new Physical.JsonSerDes())
				.setPrettyPrinting()
				.create();

	}

	public static JsonObject parse(String jsonString) {
		
		return gson.fromJson(jsonString, JsonElement.class).getAsJsonObject();
	}
	
	static public String prettyPrinting(String json) {
		try {
			JsonParser jsonParser = new JsonParser();
			JsonObject jo = jsonParser.parse(json).getAsJsonObject();
			return gsonWellPrinted.toJson(jo);
		} catch (Exception e) {
			return null;
		}
	}

	static public String from(Object o) {
		return gson.toJson(o);
	}

	public static <T> T to(String string, Class<T> c) {
		return gson.fromJson(string, c);
	}

	public static <T> T to(Message m, Class<T> c) {
		return gson.fromJson(m.getPayloadString(), c);
	}

	public static <T> T to(JsonElement json, Class<T> c) {
		return gson.fromJson(json, c);
	}
}

package ch.hevs.inuit.library.misc;


/**
 * A object that implement this can fill his data from a Object by calling fillWith
 * Used for iNUIT sensor Model
 */
public interface IFillable {
	 void fillWith(Object o);
}

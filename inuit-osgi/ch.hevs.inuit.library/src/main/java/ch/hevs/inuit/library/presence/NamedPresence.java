package ch.hevs.inuit.library.presence;

/**
 * A basic presence. contain only a name to identify it.
 */
public class NamedPresence {
	public String name;
	
	public boolean check(){
		return name != null;
	}
}

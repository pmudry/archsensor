package ch.hevs.inuit.components.timecounter;

import java.util.Timer;
import java.util.TimerTask;

import ch.hevs.inuit.library.json.BooleanBatch;
import ch.hevs.inuit.library.json.IntegerBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class TimeCounter implements IMessagingSubscriber {

	public TimeCounter(IMessagingService messaging, TimeCounterAppear appear) {
		this.appear = appear;
		this.messaging = messaging;

		if (appear.startTopic != null) {
			appear.startTopic += "/data";
			messaging.subscribe(this, appear.startTopic);
		}
		if (appear.stopTopic != null) {
			appear.stopTopic += "/data";
			messaging.subscribe(this, appear.stopTopic);
		}
		if (appear.resetTopic != null) {
			appear.resetTopic += "/data";
			messaging.subscribe(this, appear.resetTopic);
		}

		new Timer().scheduleAtFixedRate(new Refresh(), 500, 1000);
		
	}

	class Refresh extends TimerTask {
		@Override
		public void run() {
			sendTime();
		}
	}
	class Tick extends TimerTask {
		@Override
		public void run() {
			if(appear.tickTopic == null) return;
			BooleanBatch output = new BooleanBatch();
			output.t = new Long[]{System.currentTimeMillis()};
			
			messaging.send(new Message(output), appear.tickTopic + "/data");
		}
	}
	
	IMessagingService messaging;
	TimeCounterAppear appear;
	
	Timer tickTimer;

	long counter = 0;
	long startTime = 0;
	boolean enable = false;

	long getTimeCounter() {
		return enable ? counter + System.currentTimeMillis() - startTime
				: counter;
	}

	void sendTime() {
		if(appear.timeTopic == null) return;
		IntegerBatch sample = new IntegerBatch();
		sample.setValue(getTimeCounter());
		sample.updateTimeStamp();

		messaging.send(new Message(sample), appear.timeTopic + "/data");
	}

	@Override
	public void receive(String topic, Message message) {
		if (topic.equals(appear.startTopic)) {
			startTime = System.currentTimeMillis();
			enable = true;
			tickTimer = new Timer();
			tickTimer.scheduleAtFixedRate(new Tick(),appear.tickPeriodeMs,appear.tickPeriodeMs);
		}

		if (topic.equals(appear.stopTopic) && enable) {
			counter += System.currentTimeMillis() - startTime;
			enable = false;
			sendTime();
			if(tickTimer != null)tickTimer.cancel();
		}

		if (topic.equals(appear.resetTopic)) {
			enable = false;
			counter = 0;
			sendTime();
			if(tickTimer != null)tickTimer.cancel();
		}

	}

}

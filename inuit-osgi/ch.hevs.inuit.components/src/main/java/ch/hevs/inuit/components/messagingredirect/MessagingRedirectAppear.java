package ch.hevs.inuit.components.messagingredirect;

import ch.hevs.inuit.library.presence.NamedPresence;

public class MessagingRedirectAppear extends NamedPresence{	
	
	public String from,to;

	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && from != null && to != null;
	}

}

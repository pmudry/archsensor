package ch.hevs.inuit.components.internaleventintegrator;

import ch.hevs.inuit.library.event.EventSource;
import ch.hevs.inuit.library.event.IEventSink;
import ch.hevs.inuit.library.event.IEventSource;
import ch.hevs.inuit.library.misc.IFloatSource;

public class InternalEventIntegrator implements IEventSink,IFloatSource {
	int eventCount = 0;
	float integrationPerTick;
	
	public InternalEventIntegrator(EventSource eventSource,float integrationPerTick) {
		this.integrationPerTick = integrationPerTick;
		eventSource.subscribe(this);
	}

	@Override
	public float getFloatValue() {
		return eventCount*integrationPerTick;
	}

	@Override
	public void eventFrom(IEventSource source) {
		eventCount++;
	}
}
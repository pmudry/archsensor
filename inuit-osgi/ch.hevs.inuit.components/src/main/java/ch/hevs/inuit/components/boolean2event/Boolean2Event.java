package ch.hevs.inuit.components.boolean2event;

import ch.hevs.inuit.components.boolean2event.Boolean2EventAppear.Edge;
import ch.hevs.inuit.library.json.BooleanBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class Boolean2Event implements IMessagingSubscriber {
	
	public Boolean2Event(IMessagingService messaging, Boolean2EventAppear appear) {
		this.appear = appear;
		this.messaging = messaging;
		messaging.subscribe(this, appear.booleanTopic + "/data");
		switch (appear.edge) {
		case falling:
			oldState = false;
			break;
		case rising:
			oldState = true;
			break;
		}
	}
	IMessagingService messaging;
	Boolean2EventAppear appear;

	boolean oldState;

	@Override
	public void receive(String topic, Message message) {

		BooleanBatch input = BooleanBatch.from(message);
		boolean state = input.getLastValue();
		
		if (state != oldState) {
			if ((appear.edge == Edge.falling && state == false)
					|| (appear.edge == Edge.rising && state == true)) {
				
				BooleanBatch output = new BooleanBatch();
				output.t = new Long[]{System.currentTimeMillis()};
				
				messaging.send(new Message(output), appear.eventTopic + "/data");
			}
			oldState = state;
		}

	}
}

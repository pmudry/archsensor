package ch.hevs.inuit.components;

import ch.hevs.inuit.components.boolean2event.Boolean2Event;
import ch.hevs.inuit.components.boolean2event.Boolean2EventAppear;
import ch.hevs.inuit.components.eventCounter.EventCounter;
import ch.hevs.inuit.components.eventCounter.EventCounterAppear;
import ch.hevs.inuit.components.internaleventintegrator.InternalEventIntegrator;
import ch.hevs.inuit.components.internaleventintegrator.InternalEventIntegratorMqttAppear;
import ch.hevs.inuit.components.messaging.MessagingSend;
import ch.hevs.inuit.components.messagingredirect.MessagingRedirect;
import ch.hevs.inuit.components.messagingredirect.MessagingRedirectAppear;
import ch.hevs.inuit.components.periodicmessage.PeriodicMessageAppear;
import ch.hevs.inuit.components.periodicmessage.PeriodicMessage;
import ch.hevs.inuit.components.timecounter.TimeCounter;
import ch.hevs.inuit.components.timecounter.TimeCounterAppear;
import ch.hevs.inuit.components.waveplayer.WavePlayer;
import ch.hevs.inuit.components.waveplayer.WavePlayerAppear;
import ch.hevs.inuit.library.event.EventSource;
import ch.hevs.inuit.library.helper.DeviceActivatorHelper;
import ch.hevs.inuit.library.helper.SenderPeriodicFloat;
import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.json.model.Software;
import ch.hevs.inuit.library.service.Message;

public class Activator extends DeviceActivatorHelper {
    @Override
    protected boolean appear(Object o) {
        try {
            MessagingRedirectAppear appear = getAppear(o, "messagingRedirect", MessagingRedirectAppear.class);
            if (appear != null) {
                System.out.println("new messagingRedirect from " + appear.from + " to " + appear.to);

                new MessagingRedirect(messagingService, appear);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            EventCounterAppear appear = getAppear(o, "eventCounter", EventCounterAppear.class);
            if (appear != null) {
                System.out.println("new eventCounter " + appear.counterTopic);

                Model model = new Model();
                model.description = "Counter generated from (inc,dec)" + appear.incTopic + " and " + appear.decTopic;

                model.software = Software.from(getContext());
                model.fillWith(appear.model);
                messagingService.send(new Message(model), appear.counterTopic + "/model");

                new EventCounter(messagingService, appear);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            Boolean2EventAppear appear = getAppear(o, "boolean2Event", Boolean2EventAppear.class);
            if (appear != null) {
                System.out.println("new boolean2Event " + appear.eventTopic);

                Model model = new Model();
                model.description = "Event generated from " + appear.booleanTopic;

                model.software = Software.from(getContext());
                model.fillWith(appear.model);
                messagingService.send(new Message(model), appear.eventTopic + "/model");

                new Boolean2Event(messagingService, appear);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            InternalEventIntegratorMqttAppear appear = getAppear(o, "internalEventIntegrator", InternalEventIntegratorMqttAppear.class);
            if (appear != null) {
                System.out.println("new InternalEventIntegrator " + appear.interruptName);
                EventSource eventSource = (EventSource) directory.get(appear.interruptName);
                InternalEventIntegrator integrator = new InternalEventIntegrator(eventSource, appear.integrationPerTick);
                new SenderPeriodicFloat(integrator, 2000, appear.topic + "/data", messagingService);

                Model model = new Model();
                model.description = "Event integrator from " + appear.interruptName + " with a rate of " + appear.integrationPerTick + "per tick";

                model.software = Software.from(getContext());
                model.fillWith(appear.model);
                messagingService.send(new Message(model), appear.topic + "/model");

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            TimeCounterAppear appear = getAppear(o, "timeCounter", TimeCounterAppear.class);
            if (appear != null) {
                System.out.println("new TimeCounter " + appear.timeTopic);

                Model model = new Model();
                model.description = "TimeCounter";

                model.software = Software.from(getContext());
                model.fillWith(appear.model);

                messagingService.send(new Message(model), appear.timeTopic + "/model");

                new TimeCounter(messagingService, appear);
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            MessagingSend appear = getAppear(o, "messagingSend", MessagingSend.class);
            if (appear != null) {
                System.out.println("new MessagingSend " + appear.topic);

                Message m = new Message()
                        .setPayload(appear.payload)
                        .setQos(appear.qos)
                        .setRetained(appear.retain);

                messagingService.send(m, appear.topic);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try {
            WavePlayerAppear appear = getAppear(o, "wavePlayer", WavePlayerAppear.class);
            if (appear != null) {
                System.out.println("new WavePlayer " + appear.topic);

                new WavePlayer(messagingService, appear);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            PeriodicMessageAppear appear = getAppear(o, "periodicMessage", PeriodicMessageAppear.class);
            if (appear != null) {
                System.out.println("new PeriodicMessage " + appear.topic);

                new PeriodicMessage(messagingService, appear);

                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected boolean disappear(Object o) {
        return false;
    }
}

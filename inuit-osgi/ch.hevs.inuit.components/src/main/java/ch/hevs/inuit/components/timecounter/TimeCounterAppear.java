package ch.hevs.inuit.components.timecounter;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.NamedPresence;

public class TimeCounterAppear extends NamedPresence{	
	public String startTopic,stopTopic,resetTopic,timeTopic;
	
	public String tickTopic;
	public Long tickPeriodeMs = (long) 1000;
	
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && startTopic != null;
	}

}

package ch.hevs.inuit.components.messagingredirect;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class MessagingRedirect implements IMessagingSubscriber {

	public MessagingRedirect(IMessagingService messaging,
			MessagingRedirectAppear appear) {
		this.appear = appear;
		this.messaging = messaging;
		messaging.subscribe(this, appear.from);

	}

	IMessagingService messaging;
	MessagingRedirectAppear appear;

	private void send(Message message, String to, String topic)
	{
		/*System.out.println("MessagingRedirect : '" + appear.from +"->" +
				          appear.to + "' " +
				          " topic : '" + topic + "' " +
				          " to : '"+ to +"'"
				          );*/
		messaging.send(message, to);
	}
	
	@Override
	public void receive(String topic, Message message) {
		if (appear.from.endsWith("#"))
		{
			String tmp = appear.from.substring(0, appear.from.length()-2);
			String to = appear.to + topic.substring(tmp.length(), topic.length());
			
			send(message, to, topic);
			
			return;
		}
		
		if (appear.from.contains("+"))
		{
			String to = appear.to + topic.substring(appear.from.indexOf('+')-1, topic.length());
			
			send(message, to, topic);
			
			return;
		}
		
		send(message, appear.to, topic);	
	}
}

package ch.hevs.inuit.components.periodicmessage;

import ch.hevs.inuit.library.presence.NamedPresence;

public class PeriodicMessageAppear extends NamedPresence {
	public String topic;
	public String msg;
	public double period;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && topic != null && msg != null && period != 0;
	}
}

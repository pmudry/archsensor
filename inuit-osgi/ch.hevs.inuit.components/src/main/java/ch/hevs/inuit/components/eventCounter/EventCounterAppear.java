package ch.hevs.inuit.components.eventCounter;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.NamedPresence;

public class EventCounterAppear extends NamedPresence{	
	public String incTopic,decTopic,counterTopic;	
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && counterTopic != null;
	}

}

package ch.hevs.inuit.components.internaleventintegrator;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.NamedPresence;

public class InternalEventIntegratorAppear extends NamedPresence{	
	public String interruptName;
	public Float integrationPerTick;
	
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && interruptName != null && integrationPerTick != null;
	}
}

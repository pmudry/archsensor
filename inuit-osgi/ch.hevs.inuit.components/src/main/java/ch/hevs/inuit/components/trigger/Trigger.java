package ch.hevs.inuit.components.trigger;

import java.util.Timer;
import java.util.TimerTask;

import ch.hevs.inuit.library.json.IntegerBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class Trigger implements IMessagingSubscriber {

	public Trigger(IMessagingService messaging, TriggerAppear appear) {
		this.appear = appear;
		this.messaging = messaging;

		if (appear.startTopic != null) {
			appear.startTopic += "/data";
			messaging.subscribe(this, appear.startTopic);
		}
		if (appear.stopTopic != null) {
			appear.stopTopic += "/data";
			messaging.subscribe(this, appear.stopTopic);
		}
		if (appear.resetTopic != null) {
			appear.resetTopic += "/data";
			messaging.subscribe(this, appear.resetTopic);
		}

		sendTime();
		new Timer().scheduleAtFixedRate(new Refresh(), 500, 1000);
	}

	class Refresh extends TimerTask {
		@Override
		public void run() {
			if(! enable)return;
			sendTime();
		}
	}

	IMessagingService messaging;
	TriggerAppear appear;

	long counter = 0;
	long startTime = 0;
	boolean enable = false;

	long getTimeCounter() {
		return enable ? counter + System.currentTimeMillis() - startTime
				: counter;
	}

	void sendTime() {
		IntegerBatch sample = new IntegerBatch();
		sample.setValue(getTimeCounter());
		sample.updateTimeStamp();

		messaging.send(new Message(sample), appear.timeTopic + "/data");
	}

	@Override
	public void receive(String topic, Message message) {
		if (topic.equals(appear.startTopic)) {
			startTime = System.currentTimeMillis();
			enable = true;
		}

		if (topic.equals(appear.stopTopic) && enable) {
			counter += System.currentTimeMillis() - startTime;
			enable = false;
			sendTime();
		}

		if (topic.equals(appear.resetTopic)) {
			enable = false;
			counter = 0;
			sendTime();
		}

	}

}

package ch.hevs.inuit.components.eventCounter;

import ch.hevs.inuit.library.json.IntegerBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class EventCounter implements IMessagingSubscriber {

	public EventCounter(IMessagingService messaging, EventCounterAppear appear) {
		this.appear = appear;
		this.messaging = messaging;

		if(appear.incTopic  != null) {
			appear.incTopic += "/data";
			messaging.subscribe(this, appear.incTopic);
		}
		if(appear.decTopic  != null){
			appear.decTopic += "/data";
			messaging.subscribe(this, appear.decTopic);
		}

	}

	IMessagingService messaging;
	EventCounterAppear appear;

	long counter = 0;

	@Override
	public void receive(String topic, Message message) {
		if (topic.equals(appear.incTopic))
			counter++;
		if (topic.equals(appear.decTopic))
			counter--;

		IntegerBatch sample = new IntegerBatch();
		sample.setValue(counter);
		sample.updateTimeStamp();

		messaging.send(new Message(sample), appear.counterTopic + "/data");
	}
}

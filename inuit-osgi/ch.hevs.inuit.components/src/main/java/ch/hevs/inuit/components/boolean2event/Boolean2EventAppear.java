package ch.hevs.inuit.components.boolean2event;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.NamedPresence;

public class Boolean2EventAppear extends NamedPresence{	
	enum Edge{rising,falling};
	public Edge edge = Edge.rising;
	public String booleanTopic,eventTopic;
	
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && edge != null && booleanTopic != null && eventTopic != null;
	}

}

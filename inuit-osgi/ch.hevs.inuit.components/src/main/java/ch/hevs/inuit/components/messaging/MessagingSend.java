package ch.hevs.inuit.components.messaging;

import ch.hevs.inuit.library.presence.NamedPresence;

public class MessagingSend extends NamedPresence{
	public String topic;
	public String payload;
	public int qos = 0;
	public boolean retain = false;
	
	
	@Override
	public boolean check() {
		return super.check() && topic != null && payload != null;
	}
}

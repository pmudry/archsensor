package ch.hevs.inuit.components.waveplayer;

import java.io.IOException;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

public class WavePlayer implements IMessagingSubscriber {

	public WavePlayer(IMessagingService messaging,WavePlayerAppear appear) {
		this.appear = appear;
		messaging.subscribe(this, appear.topic + "/data");
	}
	
	WavePlayerAppear appear;
	
	@Override
	public void receive(String topic, Message message) {
		
		try {
	//		String [] args = 
			Runtime.getRuntime().exec("/usr/bin/aplay " + appear.sound);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

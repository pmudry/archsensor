package ch.hevs.inuit.components.waveplayer;

import ch.hevs.inuit.library.presence.NamedPresence;

public class WavePlayerAppear extends NamedPresence{	
	public String sound;
	public String topic;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && sound != null && topic != null;
	}

}

package ch.hevs.inuit.components.trigger;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.NamedPresence;

public class TriggerAppear extends NamedPresence{	
	public String startTopic,stopTopic,resetTopic,timeTopic;	
	public Model model;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && timeTopic != null && startTopic != null && stopTopic != null && resetTopic != null;
	}

}

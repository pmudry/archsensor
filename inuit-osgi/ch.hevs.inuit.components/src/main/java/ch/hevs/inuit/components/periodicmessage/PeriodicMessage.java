package ch.hevs.inuit.components.periodicmessage;

import java.util.Timer;
import java.util.TimerTask;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.Message;

public class PeriodicMessage {

	protected String topic;
	protected String msg;
	protected double period;
	protected int counter = 0;
	protected IMessagingService ms;

	public PeriodicMessage(IMessagingService messagingService,
			PeriodicMessageAppear appear) {
		this.topic = appear.topic;
		this.msg = appear.msg;
		this.period = appear.period;
		this.ms = messagingService;

		System.err.println(this.getClass() + " :" + this.msg);

		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {	
				ms.send(new Message(msg + " (" + counter++ + ")"), topic);
			}
		}, (long) (period * 1000), (long) (period * 1000));
	}
}

package ch.hevs.inuit.sensor.ad7991;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import ch.hevs.inuit.library.i2c.II2cDevice;
import ch.hevs.inuit.library.json.NumberBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.Message;
import ch.hevs.inuit.sensor.ad7991.Ad7991Appear.Channel;

public class Ad7991Messaging extends Ad7991 {
	Timer timer;
	IMessagingService client;
	Channel[] channels;

	public Ad7991Messaging(II2cDevice device,long periodMs,Channel[] channels, IMessagingService client)
			throws IOException {
		super(device);
		this.channels = channels;
		this.client = client;

		timer = new Timer();
		timer.scheduleAtFixedRate(new Task(), 0, periodMs);
	}

	class Task extends TimerTask {
		@Override
		public void run() {
			float[] channelsValue = readChannels();
			for (int idx = 0; idx < channels.length; idx++) {
				channelsValue[idx] = (float) (channelsValue[idx]*channels[idx].factor + channels[idx].offset);
				
				NumberBatch sample = new NumberBatch();
				String topicAd = channels[idx].topic + "/data";

				sample.setValue((double)channelsValue[idx]);
				sample.updateTimeStamp();
				client.send(new Message(sample), topicAd);
			}
		}
	}

	@Override
	public void destructor() {
		timer.cancel();
		super.destructor();
	}
}

package ch.hevs.inuit.sensor.ad7991;

import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.presence.I2cDeviceAppear;

public class Ad7991Appear extends I2cDeviceAppear{
	public Float period;
	public Channel[] channels;

	public static class Channel{
		public String topic;
		public Model model;		
		
		//Value = (0 < sampled value < 1)*factor + offset
		public Double offset = 0.0;
		public Double factor = 1.0; 
		
		public boolean check(){
			return topic != null;
		}
	}
	
	@Override
	public boolean check() {
		if(super.check() == false || period == null || channels == null || channels.length > 4) return false;
		
		for(Channel c : channels){
			if(c == null || c.check() == false) return false;
		}
		
		return true;
	}
}

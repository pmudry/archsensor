package ch.hevs.inuit.sensor.ad7991;

import java.io.IOException;

import ch.hevs.inuit.library.i2c.II2cDevice;

public class Ad7991 {

	final II2cDevice device;

	public Ad7991(II2cDevice device) throws IOException {
		this.device = device;
		byte[] buf = new byte[] { (byte) 0xF0 };
		device.write(buf, 0, 1);
	}

	float[] readChannels() {
		byte[] buf = new byte[16];
		try {
			device.read(buf, 0, 2);
			device.read(buf, 2, 2);
			device.read(buf, 4, 2);
			device.read(buf, 6, 2);
		} catch (IOException e) {
		}

		float[] ret = new float[] { getValue(buf, 0), getValue(buf, 2),
				getValue(buf, 4), getValue(buf, 6) };
		return ret;
	}

	float getValue(byte[] buf, int offset) {
		int temp = 0;
		temp += S8toU8(buf[offset + 0]) << 8;
		temp += S8toU8(buf[offset + 1]);
		temp &= 0xFFF;
		return temp / 4095f;
	}

	int S8toU8(int value) {
		return value < 0 ? value + 256 : value;
	}

	public void destructor() {

	}

}

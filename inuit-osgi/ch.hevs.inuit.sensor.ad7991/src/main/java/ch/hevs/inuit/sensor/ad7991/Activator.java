package ch.hevs.inuit.sensor.ad7991;

import org.osgi.framework.BundleContext;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;
import ch.hevs.inuit.library.i2c.II2cBus;
import ch.hevs.inuit.library.json.model.Accuracy;
import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.json.model.Physical;
import ch.hevs.inuit.library.json.model.Refresh;
import ch.hevs.inuit.library.json.model.Sampling;
import ch.hevs.inuit.library.json.model.Software;
import ch.hevs.inuit.library.service.Message;
import ch.hevs.inuit.sensor.ad7991.Ad7991Appear.Channel;

public class Activator extends DeviceActivatorHelper {
	
	
	public static Model newModel(BundleContext context,double period, int channelId, Channel channel){
		Model model = new Model()
			.setDescription("Quad channel analog to digital converter (channel : " + channelId + ")")
			.setName("AD7991");

		double resolution = 1.0/4096*Math.abs(channel.factor);
		
		model.sampling = new Sampling()
			.setPhysical(new Physical.None())
			.setRefresh(new Refresh.Periodic().setPeriod(period))
			.setAccuracy(new Accuracy.Absolute(resolution*10,resolution));
		
		model.software = Software.from(context);


		return model;
	}
	
	@Override
	protected boolean appear(Object o) {
		try {
			Ad7991Appear appear = getAppear(o, "AD7991", Ad7991Appear.class);
			if (appear != null) {
				System.out.println("new AD7991");
						
				int channelId = 0;
				for(Ad7991Appear.Channel channel : appear.channels){
					Model model = newModel(getContext(),appear.period,channelId++,channel);
					model.fillWith(channel.model);							
					messagingService.send(new Message(model), channel.topic + "/model");
				}
				
				new Ad7991Messaging(
						((II2cBus) directory.get(appear.busName)).getDevice(appear.address),
						(long) (appear.period * 1000),
						appear.channels,
						messagingService);
				
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object o) {
		return false;
	}


}

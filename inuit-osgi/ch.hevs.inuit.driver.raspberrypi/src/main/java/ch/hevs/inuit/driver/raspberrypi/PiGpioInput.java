package ch.hevs.inuit.driver.raspberrypi;

import ch.hevs.inuit.library.json.BooleanBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.Message;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.GpioInterrupt;
import com.pi4j.wiringpi.GpioInterruptEvent;
import com.pi4j.wiringpi.GpioInterruptListener;
import com.pi4j.wiringpi.GpioUtil;

public class PiGpioInput {
	private IMessagingService mqttService;
	private String topic;
	int pinId;

	PiGpioInput(IMessagingService mqttService, String topic, final int pinId) {
		this.mqttService = mqttService;
		this.topic = topic;
		this.pinId = pinId;

		GpioInterrupt.addListener(new GpioInterruptListener() {
			@Override
			public void pinStateChange(GpioInterruptEvent event) {
				if (event.getPin() == pinId) {
					sendState(event.getState());
				}
			}
		});

		// export all the GPIO pins that we will be using
		GpioUtil.export(pinId, GpioUtil.DIRECTION_IN);

		// set the edge state on the pins we will be listening for
		GpioUtil.setEdgeDetection(pinId, GpioUtil.EDGE_BOTH);

		// configure GPIO 0 as an INPUT pin; enable it for callbacks
		Gpio.pinMode(pinId, Gpio.INPUT);
		Gpio.pullUpDnControl(pinId, Gpio.PUD_DOWN);
		GpioInterrupt.enablePinStateChangeCallback(pinId);

	}

	void sendState(boolean state) {
		BooleanBatch sample = new BooleanBatch();
		sample.setValue(state);
		sample.updateTimeStamp();

		PiGpioInput.this.mqttService.send(new Message(sample).setAsEventStateQos(), PiGpioInput.this.topic);
	}
}

package ch.hevs.inuit.driver.raspberrypi;

import java.io.IOException;

import org.osgi.framework.BundleContext;

import com.pi4j.wiringpi.Gpio;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;
import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.json.model.Software;
import ch.hevs.inuit.library.presence.GpioAppear;
import ch.hevs.inuit.library.presence.GpioMqttAppear;
import ch.hevs.inuit.library.presence.MessagingPresence;
import ch.hevs.inuit.library.service.Message;

public class Activator extends DeviceActivatorHelper {
	@Override
	public void start(BundleContext bundleContext) throws Exception {

		boolean success = false;
		
        // setup wiringPi
		try {
	        if (Gpio.wiringPiSetup() == -1) {
	            System.out.println(" ==>> GPIO SETUP FAILED");
	        }
	        success = true;
		}
		catch (java.lang.UnsatisfiedLinkError e)
		{
			System.err.println("failed to load the raspberry pi driver. If running on another platform, this is absolutely normal. ");
			//e.printStackTrace();
		}
		
		if (success) {
			super.start(bundleContext);
		}
	}
	
	@Override
	protected boolean appear(Object o) {
		try {

			PiI2cAppear appear = getAppear(o, "piI2cBus", PiI2cAppear.class);
			if (appear != null) {
				System.out.println("new PiI2cDriver " + appear.busId);

				I2cBusImpl piI2cBus = new I2cBusImpl(appear.busId);
				directory.put("piI2cBus" + appear.busId, piI2cBus);
				
				return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			GpioAppear appear = getAppear(o, "piGpioInterrupt", GpioAppear.class);
			if (appear != null) {
				System.out.println("new piGpioInterrupt " + appear.pinId);		
				
				PiGpioInterrupt piGpioInterrupt = new PiGpioInterrupt(appear.pinId);
				directory.put("gpioInterrupt" + appear.pinId, piGpioInterrupt);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			GpioMqttAppear appear = getAppear(o, "piGpioOutput", GpioMqttAppear.class);
			if (appear != null) {
				System.out.println("new piGpioOutput " + appear.pinId);
				
				Model model = new Model();
				model.description = "Pi gpio output " + appear.pinId;
				
				model.software = Software.from(getContext());
				model.fillWith(appear.model);							
				messagingService.send(new Message(model), appear.topic + "/model");				
				
				PiGpioOutput piGpioOutput = new PiGpioOutput(messagingService,appear.topic + "/data",appear.pinId);
				directory.put("gpioOutput" + appear.pinId, piGpioOutput);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			GpioMqttAppear appear = getAppear(o, "piGpioInput", GpioMqttAppear.class);
			if (appear != null) {
				System.out.println("new piGpioInput " + appear.pinId);
						
				Model model = new Model();
				model.description = "Pi gpio input " + appear.pinId;
				
				model.software = Software.from(getContext());
				model.fillWith(appear.model);							
				messagingService.send(new Message(model), appear.topic + "/model");	
				
				PiGpioInput piGpioInput = new PiGpioInput(messagingService,appear.topic + "/data",appear.pinId);
				directory.put("gpioInput" + appear.pinId, piGpioInput);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
		try {
			MessagingPresence appear = getAppear(o, "piFace", MessagingPresence.class);
			if (appear != null) {
				System.out.println("new piFace");
				
				new MqttPiFace(messagingService,appear.topic);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object o) {
		return false;
	}
}

package ch.hevs.inuit.driver.raspberrypi;

import java.io.IOException;

import ch.hevs.inuit.library.i2c.II2cBus;
import ch.hevs.inuit.library.i2c.II2cDevice;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class I2cBusImpl implements II2cBus {

	final I2CBus bus;
	
	public I2cBusImpl(int busId) throws IOException {
		 bus = I2CFactory.getInstance(busId);
	}
	
	@Override
	public II2cDevice getDevice(int address) throws IOException {
		I2CDevice device = bus.getDevice(address);
		return new I2cDeviceImpl(device);
	}

	public void destructor() {
		try {
			bus.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
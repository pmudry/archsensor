package ch.hevs.inuit.driver.raspberrypi;

import java.io.IOException;

import ch.hevs.inuit.library.event.EventSource;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.GpioInterrupt;
import com.pi4j.wiringpi.GpioInterruptEvent;
import com.pi4j.wiringpi.GpioInterruptListener;
import com.pi4j.wiringpi.GpioUtil;

public class PiGpioInterrupt extends EventSource{
	int pinId;

	public PiGpioInterrupt(final int pinId) throws IOException {
		this.pinId = pinId;
		
		GpioInterrupt.addListener(new GpioInterruptListener() {
			@Override
			public void pinStateChange(GpioInterruptEvent event) {
				if(event.getPin() == pinId){
					System.out.println("EVENT on " + pinId);
					pushEvent();
				}
			}
		});	

        // export all the GPIO pins that we will be using
        GpioUtil.export(pinId, GpioUtil.DIRECTION_IN);

        
        // set the edge state on the pins we will be listening for
        GpioUtil.setEdgeDetection(pinId, GpioUtil.EDGE_RISING);
        
        // configure GPIO 0 as an INPUT pin; enable it for callbacks
        Gpio.pinMode(pinId, Gpio.INPUT);
        Gpio.pullUpDnControl(pinId, Gpio.PUD_DOWN);        
        GpioInterrupt.enablePinStateChangeCallback(pinId);
	}
	
	public void destructor() {
		
	}
}
package ch.hevs.inuit.driver.raspberrypi;

import ch.hevs.inuit.library.presence.NamedPresence;

public class PiI2cAppear extends NamedPresence{

	public Integer busId;
	
	@Override
	public boolean check() {
		return super.check() && busId != null;
	}
}

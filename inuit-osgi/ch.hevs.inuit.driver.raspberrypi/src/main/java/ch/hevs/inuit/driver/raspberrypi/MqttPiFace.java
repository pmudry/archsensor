package ch.hevs.inuit.driver.raspberrypi;

import java.io.IOException;
import java.util.Timer;
import ch.hevs.inuit.library.json.BooleanBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

import com.pi4j.component.relay.RelayState;
import com.pi4j.component.switches.SwitchListener;
import com.pi4j.component.switches.SwitchState;
import com.pi4j.component.switches.SwitchStateChangeEvent;
import com.pi4j.device.piface.PiFace;
import com.pi4j.device.piface.impl.PiFaceDevice;
import com.pi4j.wiringpi.Spi;

public class MqttPiFace implements IMessagingSubscriber {

	IMessagingService mqttService;
	Timer timer;

	String rootTopic;

	PiFace piface;
	boolean[] oldDigitalInput = new boolean[4];

	public MqttPiFace(IMessagingService mqttService, String rootTopic) {
		rootTopic += "/";
		this.mqttService = mqttService;

		this.rootTopic = rootTopic;

		try {
			piface = new PiFaceDevice(PiFace.DEFAULT_ADDRESS, Spi.CHANNEL_0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		 for (int idx = 0; idx < 4; idx++) { new switchListenerMqtt(idx); }
		 
		mqttService.subscribe(this, rootTopic + "#");

		//timer = new Timer();
		//timer.scheduleAtFixedRate(new Task(), 0, 200);
	}

	public class switchListenerMqtt implements SwitchListener {
		int id;

		switchListenerMqtt(int id) {
			this.id = id;
			piface.getSwitch(id).addListener(this);
		}

		@Override
		public void onStateChange(SwitchStateChangeEvent event) {
			try {
				BooleanBatch sample = new BooleanBatch();
				String topic = rootTopic + "button/" + id + "/data";

				sample.setValue(event.getNewState() == SwitchState.ON);
				sample.updateTimeStamp();
				mqttService.send(new Message(sample).setAsEventStateQos(), topic);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public void destructor() {
		timer.cancel();
		mqttService.unsubscribe(this, rootTopic + "#");
	}

//	class Task extends TimerTask {
//		boolean boot = true;
//
//		@Override
//		public void run() {
//			for (int idx = 0; idx < 4; idx++) {
//				GpioPinDigitalInput input = piface.getInputPins()[idx];
//				boolean value = input.getState() == PinState.LOW;
//				if (boot || oldDigitalInput[idx] != value) {
//					String topic = rootTopic + "button/" + idx + "/data";
//					BooleanBatch sample = new BooleanBatch();
//					sample.setValue(value);
//					sample.updateTimeStamp();
//
//					mqttService.send(new Message(sample).setAsEventStateQos(), topic);
//				}
//
//				oldDigitalInput[idx] = value;
//			}
//			boot = false;
//		}
//	}

	@Override
	public void receive(String topic, Message message) {
		try {
			if (topic.startsWith(rootTopic) == false)
				return;

			topic = topic.replace(rootTopic, "");
			String[] split = topic.split("/");

			if (split[0].equals("led")) {
				int ledId = Integer.parseInt(split[1]);
				if (split[2].equals("data")) {
					BooleanBatch sample = BooleanBatch.from(message);
					if (sample.getLastValue())
						piface.getLed(ledId).on();
					else
						piface.getLed(ledId).off();
				}
			}
			if (split[0].equals("relay")) {
				int relayId = Integer.parseInt(split[1]);
				if (split[2].equals("data")) {
					BooleanBatch sample = BooleanBatch.from(message);
					if (sample.getLastValue())
						piface.getRelay(relayId).setState(RelayState.CLOSED);
					else
						piface.getRelay(relayId).setState(RelayState.OPEN);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
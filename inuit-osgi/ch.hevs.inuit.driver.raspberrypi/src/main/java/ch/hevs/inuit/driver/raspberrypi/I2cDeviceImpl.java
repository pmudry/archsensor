package ch.hevs.inuit.driver.raspberrypi;

import java.io.IOException;

import ch.hevs.inuit.library.i2c.II2cDevice;

import com.pi4j.io.i2c.I2CDevice;

public class I2cDeviceImpl implements II2cDevice{
	private I2CDevice device;

	public I2cDeviceImpl(I2CDevice device) {
		this.device = device;
	}

	@Override
	public void write(int address, byte[] buffer, int offset, int size)
			throws IOException {
		device.write(address, buffer, offset, size);
	}

	@Override
	public int read(int address, byte[] buffer, int offset, int size)
			throws IOException {
		return device.read(address, buffer, offset, size);
	}
	@Override
	public int read(byte[] buffer, int offset, int size)
			throws IOException {
		return device.read(buffer, offset, size);
	}

	@Override
	public void write(byte[] buf, int offset, int size) throws IOException {
		device.write(buf, offset, size);
	}
}

package ch.hevs.inuit.driver.raspberrypi;

import ch.hevs.inuit.library.json.BooleanBatch;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.GpioUtil;

public class PiGpioOutput implements IMessagingSubscriber {
	private IMessagingService mqttService;
	private String topic;
	private int pinId;

	PiGpioOutput(IMessagingService _mqttService, String _topic, int pinId) {
		this.mqttService = _mqttService;
		this.topic = _topic;
		this.pinId = pinId;

		GpioUtil.export(pinId, GpioUtil.DIRECTION_OUT);
		Gpio.pinMode(pinId, Gpio.OUTPUT);
		
		mqttService.subscribe(this, topic);
	}

	@Override
	public void receive(String topic, Message message) {
		BooleanBatch sample = BooleanBatch.from(message);
		Gpio.digitalWrite(pinId, sample.getLastValue());
	}
}

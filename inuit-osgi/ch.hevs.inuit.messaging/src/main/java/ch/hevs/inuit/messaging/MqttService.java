package ch.hevs.inuit.messaging;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;
import ch.hevs.inuit.messaging.cliententry.ClientEntry;

public class MqttService implements IMessagingService {

	// Broker array
	ArrayList<ClientEntry> clientsEntry = new ArrayList<ClientEntry>();

	/**
	 * Add a new broker to the messaging service
	 * 
	 * @param name
	 *            without the last slash
	 */
	public void addClientFactory(String name, IClientFactory factory) throws MqttException {
		ClientEntry c = new ClientEntry(factory, name, this);
		clientsEntry.add(c);
		subscriptionsMutex.lock();
		for (Subscription s : subscriptions) {
			String topic = isClientEntryInRange(c, s.topicSplit);
			if (topic != null)
				c.subscribe(topic);
		}
		subscriptionsMutex.unlock();
	}


	/**
	 * @param split
	 *            the splited form of a topic
	 * @return if the ClientEntry is at the root of this path
	 */
	String isClientEntryInRange(ClientEntry c, String[] split) {
		int level = 0;
		String[] cSplit = splitTopic(c.topic);
		while (true) {
			if (cSplit.length == level) { // OK
				String topic = "";
				while (level != split.length) {
					topic += split[level];
					if (level + 1 != split.length)
						topic += "/";
					level++;
				}
				return topic;
			}
			if (split[level].equals("#")) { // OK
				return "#";
			}
			if ((!cSplit[level].equals(split[level])) && (!split[level].equals("+"))) {
				break;// NAK
			}

			// Continue
			level++;
		}
		return null;
	}

	// A little bit usless XD
	private String[] splitTopic(String topic) {
		return topic.split("/");
	}

	/**
	 * Used to store subscription
	 */
	static class Subscription {
		public Subscription(IMessagingSubscriber subscriber, String topic) {
			this.subscriber = subscriber;
			this.topic = topic;
			this.topicSplit = topic.split("/");
		}

		public boolean equals(Object o) {
			if (o instanceof Subscription == false)
				return false;
			Subscription other = (Subscription) o;
			return other.topic.equals(topic) && other.subscriber == subscriber;
		}

		IMessagingSubscriber subscriber;
		String topic;
		String[] topicSplit; // Splited form of the topic
	}

	Lock subscriptionsMutex = new ReentrantLock(true);
	ArrayList<Subscription> subscriptions = new ArrayList<MqttService.Subscription>();

	
	/**
	 * IMessagingService
	 */
	@Override
	public void subscribe(IMessagingSubscriber subscriber, String topic) {
		String[] split = splitTopic(topic);
		for (ClientEntry c : clientsEntry) {
			String topicLeft = isClientEntryInRange(c, split);
			if (topicLeft != null)
				c.subscribe(topicLeft);
		}
		subscriptionsMutex.lock();
		subscriptions.add(new Subscription(subscriber, topic));
		subscriptionsMutex.unlock();
	}
	
	/**
	 * IMessagingService
	 */
	@Override
	public void unsubscribe(IMessagingSubscriber subscriber, String topic) {
		String[] split = splitTopic(topic);
		for (ClientEntry c : clientsEntry) {
			String topicLeft = isClientEntryInRange(c, split);
			if (topicLeft != null)
				c.unsubscribe(topicLeft);
		}

		subscriptionsMutex.lock();
		subscriptions.remove(new Subscription(subscriber, topic));
		subscriptionsMutex.unlock();
	}

	/**
	 * Called from ClientEntry
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String[] split = topic.split("/");

		//Because a messageArrived could change subscriptions
		ArrayList<Subscription> hits = new ArrayList<MqttService.Subscription>(); 
		
		//Fill hits
		for (Subscription s : getPossibleSubscriberFor(topic)) {
			int level = -1;
			boolean success = true;
			for (String key : s.topicSplit) {
				level++;
				if (key.equals("#"))
					break;
				if (key.equals("+"))
					continue;
				if (split.length > level && key.equals(split[level]))
					continue;

				success = false;
				break;
			}
			if (success) {
				hits.add(s);
			}
		}

		//feed hits's subscription
		for (Subscription s : hits) {
			try {
				s.subscriber.receive(topic, mqttToInuit(message));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Translate paho mqtt message to iNUIT Message
	 */
	Message mqttToInuit(MqttMessage mqtt) {
		return new Message().setPayload(mqtt.getPayload()).setQos(mqtt.getQos()).setRetained(mqtt.isRetained());
	}
	
	/**
	 * Translate iNUIT Message to paho mqtt message
	 */
	MqttMessage inuitToMqtt(Message inuit) {
		MqttMessage mqtt = new MqttMessage();
		mqtt.setPayload(inuit.getPayload());
		mqtt.setQos(inuit.getQos());
		mqtt.setRetained(inuit.isRetained());
		return mqtt;
	}

	ArrayList<Subscription> getPossibleSubscriberFor(String topic) {
		return subscriptions;
	}

	public void send(MqttMessage message, String topic) {
		//Dispatch to ClientEntry
		for (ClientEntry ce : clientsEntry) {
			if (topic.startsWith(ce.topic)) {
				ce.publish(topic.substring(ce.topic.length() + 1, topic.length()), message);
			}
		}
	}


	/**
	 * IMessagingService
	 */
	@Override
	public void send(Message message, String topic) {
		send(inuitToMqtt(message), topic);
	}

}
package ch.hevs.inuit.messaging.cliententry;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import ch.hevs.inuit.messaging.cliententry.ClientEntry.PendingPublish;

public class PublishTask implements ITask {

	public PublishTask(ClientEntry c, String messageTopic, MqttMessage message) {
		this.c = c;
		this.messageTopic = messageTopic;
		this.message = message;
	}

	ClientEntry c;
	String messageTopic;
	MqttMessage message;

	@Override
	public void run() {
		// System.out.println("PublishTask start");
		try {
			c.client.publish(messageTopic, message);
			// System.out.println("PublishTask sucess");
		} catch (Exception e) {
			if (message.getQos() > 0)
				c.pendingPublish.add(new PendingPublish(messageTopic, message));
			if(c.pendingPublish.size() > c.pendingPublishSizeMax) c.pendingPublish.pop();
		}
	}

}

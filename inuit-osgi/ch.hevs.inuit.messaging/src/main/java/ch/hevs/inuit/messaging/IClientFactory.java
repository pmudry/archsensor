package ch.hevs.inuit.messaging;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

/**
 * Used by ClientEntry
 */
public interface IClientFactory {
	MqttClient newClient()throws MqttException;
	void connect(MqttClient client) throws MqttSecurityException, MqttException;
}

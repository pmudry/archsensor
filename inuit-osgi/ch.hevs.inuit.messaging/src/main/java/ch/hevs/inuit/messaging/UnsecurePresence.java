package ch.hevs.inuit.messaging;

import ch.hevs.inuit.library.presence.NamedPresence;

/**
 * Used by Activator to be notified that it must create a new UnsecureClientFactory with that parameter
 */
public class UnsecurePresence extends NamedPresence{	
	String ip;
	String brokerName;
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && ip != null &&  brokerName != null;
	}
}

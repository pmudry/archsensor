package ch.hevs.inuit.messaging.cliententry;


public class ReconnectTask implements ITask {

	public ReconnectTask(ClientEntry c) {
		this.c = c;
	}

	ClientEntry c;

	@Override
	public void run() {
		c.connect();
	}

}

package ch.hevs.inuit.messaging.cliententry;

import java.util.Timer;
import java.util.TimerTask;

public class DelayedTask {
	public DelayedTask(ClientEntry c,ITask t,long msDelay) {
		
		new Timer().schedule(new TimerTask() {
			TimerTask constructor(ClientEntry c,ITask t){
				this.c = c;
				this.t = t;
				return this;
			}
			ClientEntry c;
			ITask t;
			
			@Override
			public void run() {
				c.pushTask(t);
			}
		}.constructor(c, t), msDelay);
	}
}

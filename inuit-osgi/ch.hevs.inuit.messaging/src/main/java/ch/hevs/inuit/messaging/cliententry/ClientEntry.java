package ch.hevs.inuit.messaging.cliententry;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import ch.hevs.inuit.messaging.IClientFactory;
import ch.hevs.inuit.messaging.MqttService;

/**
 * Each broker registered into MqttService has his ClientEntry The ClientEntry
 * must maintain the connection with the broker and if this connection is lost
 * it must recreate one with the same state (subscribe / pending TX packet)
 * 
 * Because multiple thread interact (multiple clients + 3 paho thread), a ITask
 * queue solve all concurrency issue.
 */

public class ClientEntry implements MqttCallback {
	public ClientEntry(IClientFactory clientFactory, String topic, MqttService mqttService) {
		this.factory = clientFactory;
		this.topic = topic;
		this.mqttService = mqttService;

		new TaskThread().start();
	}

	/**
	 * Fed by tasks queue
	 */
	class TaskThread extends Thread {
		@Override
		public void run() {
			super.run();
			connect();

			// Waiting for task and execute it
			while (true) {
				try {
					// System.out.println("[TaskThread] wait task");
					ITask t = tasks.take();
					// System.out.println("[TaskThread] do " +
					// t.getClass().getName());
					t.run();
					if (t instanceof DestructorTask)
						break;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	MqttService mqttService;
	public String topic;
	MqttClient client;
	IClientFactory factory;

	LinkedBlockingQueue<ITask> tasks = new LinkedBlockingQueue<ITask>();

	/**
	 * Because more than one subscriptions can come to the same topic, it's
	 * necessary to count it to know when you can unsubscribe
	 */
	HashMap<String, Integer> subscriptions = new HashMap<String, Integer>();

	// Retain packet with Qos > 0 when the client is unconnected with the broker
	final int pendingPublishSizeMax = 100;
	LinkedList<PendingPublish> pendingPublish = new LinkedList<PendingPublish>();

	public static class PendingPublish {
		public PendingPublish(String messageTopic, MqttMessage message) {
			this.messageTopic = messageTopic;
			this.message = message;
		}

		String messageTopic;
		MqttMessage message;
	}

	void connect() {
		if (client != null) {
			// Get pending packet from the client and push it into
			// pendingPublish if Qos > 0

			try { // client.getPendingDeliveryTokens() can return exeption XD
				for (IMqttDeliveryToken token : client.getPendingDeliveryTokens()) {
					try {
						if (token.getMessage().getQos() > 0)
							pendingPublish.addFirst(new PendingPublish(token.getTopics()[0], token.getMessage()));
					} catch (MqttException e) {
						e.printStackTrace();
					}
					if (pendingPublish.size() > pendingPublishSizeMax)
						break;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		// Create new client and configure it
		try {
			client = factory.newClient();
			factory.connect(client);
			client.setCallback(this);
			// client.setTimeToWait(200);

			// Restore subscription to the client
			for (String subscribeTopic : subscriptions.keySet()) {
				client.subscribe(subscribeTopic);
			}

			System.out.println("MQTT " + topic + " Connection success");

			// Send pending message to the broker
			for (PendingPublish pending : pendingPublish) {
				try {
					client.publish(pending.messageTopic, pending.message);
				} catch (Exception e) {
				}
			}
			pendingPublish.clear();

		} catch (Exception e) {
			System.out.println("MQTT " + topic + " Connection error");
			try {
				client.close();
			} catch (Exception e1) {
			}

			new DelayedTask(this, new ReconnectTask(this), 1000);
		}
	}

	/**
	 * Paho mqtt call back
	 */
	@Override
	public void connectionLost(Throwable arg0) {
		System.out.println("MQTT connection lost !");
		new DelayedTask(this, new ReconnectTask(this), 1000);
	}

	/**
	 * Paho mqtt call back
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

	long lastMessageArrivedDate = 0;
	int lastMessageArrivedHashCode = 0;
	String lastMessageTopic = null;

	/**
	 * Paho mqtt call back
	 */
	@Override
	public void messageArrived(String messageTopic, MqttMessage message)
			throws Exception {
		int hashCode = java.util.Arrays.hashCode(message.getPayload());
		long date = System.currentTimeMillis();
		// Avoid multiple reception of same message
		if (       hashCode != lastMessageArrivedHashCode
				|| date - lastMessageArrivedDate > 100
				|| !messageTopic.equals(lastMessageTopic)) {
			// System.out.println("***[MQTT]*** messageArrived " +
			// messageTopic);
			mqttService.messageArrived(topic + "/" + messageTopic, message);
		}else{
			//System.out.println("***[MQTT]*** throw message");
		}
		lastMessageArrivedDate = date;
		lastMessageArrivedHashCode = hashCode;
		lastMessageTopic = messageTopic;
	}

	// ************ ITask wrapper ************

	public void destructor() {
		pushTask(new DestructorTask(this));
	}

	public void subscribe(String topic) {
		pushTask(new SubscribeTask(this, topic));
	}

	public void unsubscribe(String topic) {
		pushTask(new UnsubscribeTask(this, topic));
	}

	public void publish(String messageTopic, MqttMessage message) {
		pushTask(new PublishTask(this, messageTopic, message));
	}

	void pushTask(ITask task) {
		tasks.add(task);
	}
}
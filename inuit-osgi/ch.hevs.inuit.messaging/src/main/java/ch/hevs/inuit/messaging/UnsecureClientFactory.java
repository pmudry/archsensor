package ch.hevs.inuit.messaging;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

public class UnsecureClientFactory implements IClientFactory {
	String ip;

	String user;
	String password;

	public UnsecureClientFactory(String ip) {
		this.ip = ip;
	}
	/**
	 * IClientFactory
	 */
	@Override
	public MqttClient newClient() throws MqttException {
		MqttClient client = new MqttClient("tcp://" + ip, MqttClient.generateClientId(),null);
		return client;
	}
 

	/**
	 * IClientFactory
	 */
	@Override
	public void connect(MqttClient client) throws MqttSecurityException, MqttException {
		client.connect(getOptions());
	}

	public MqttConnectOptions getOptions() {
		MqttConnectOptions options = new MqttConnectOptions();
		
		options.setCleanSession(true);
	
		if (user != null)
			options.setUserName(user);
		if (password != null)
			options.setPassword(password.toCharArray());
		

		return options;
	}

}

package ch.hevs.inuit.messaging.cliententry;

import org.eclipse.paho.client.mqttv3.MqttException;

public class SubscribeTask implements ITask{

	public SubscribeTask(ClientEntry c, String topic) {
		this.c = c;
		this.topic = topic;
	}
	
	ClientEntry c;
	String topic;
	
	@Override
	public void run() {
		Integer count = c.subscriptions.get(topic);
		if (count == null)
			count = 0;
		count++;	
		c.subscriptions.put(topic, count);		
		//subscribe to the broker
		
		try {
			c.client.subscribe(topic);
		} catch (MqttException e) {
			//e.printStackTrace();
		}	
	}

}

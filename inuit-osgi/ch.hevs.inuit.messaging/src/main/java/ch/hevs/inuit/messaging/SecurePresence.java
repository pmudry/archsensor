package ch.hevs.inuit.messaging;



/**
 * Used by Activator to be notified that it must create a new UnsecureClientFactory with that parameter
 */
public class SecurePresence extends UnsecurePresence{	

	String cafile,certfile,keyfile,certpassword;
	
	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return super.check() && ip != null &&  brokerName != null && cafile != null && certpassword != null && certfile != null && keyfile != null;
	}
}

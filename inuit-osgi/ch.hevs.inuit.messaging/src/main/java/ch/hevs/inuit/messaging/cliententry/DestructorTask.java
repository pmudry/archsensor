package ch.hevs.inuit.messaging.cliententry;

import org.eclipse.paho.client.mqttv3.MqttException;

public class DestructorTask implements ITask {

	public DestructorTask(ClientEntry c) {
		this.c = c;
	}

	ClientEntry c;

	@Override
	public void run() {
		if (c.client != null) {
			c.client.setCallback(null);
			try {
				c.client.disconnect();
			} catch (MqttException e) {
			}
		}
	}

}

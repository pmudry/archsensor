package ch.hevs.inuit.messaging;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import ch.hevs.inuit.library.misc.Utils;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IPresenceManager;
import ch.hevs.inuit.library.service.IPresenceObserver;

/**
 * Activator of the MqttMessaging service
 * Mqtt broker can be added by broadcasting on the
 * PresenceManger a ch.hevs.inuit.messaging.UnsecurePresence json notification.
 * Then this broker is added to messaging paths
 */
public class Activator implements BundleActivator {
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	MqttService mqttService;
	ServiceRegistration mqttServiceRegistration;

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;

		//Boot the service
		mqttService = new MqttService();
		mqttServiceRegistration = bundleContext.registerService(IMessagingService.class.getName(), mqttService, null);

		ServiceReference presenceManagerReference = bundleContext.getServiceReference(IPresenceManager.class.getName());
		IPresenceManager presenceManager = (IPresenceManager) bundleContext.getService(presenceManagerReference);

		
		//Listen presence manager
		presenceManager.subscribe(new IPresenceObserver() {
			@Override
			public boolean appear(Object o) {
				try {
					UnsecurePresence appear = Utils.getAppear(o, "unsecureMqttBroker", UnsecurePresence.class);
					if (appear != null) {
						System.out.println("new UnsecureMqttBroker " + appear.brokerName);
						mqttService.addClientFactory(appear.brokerName, new UnsecureClientFactory(appear.ip));
						
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					SecurePresence appear = Utils.getAppear(o, "secureMqttBroker", SecurePresence.class);
					if (appear != null) {
						System.out.println("new SecureMqttBroker " + appear.brokerName);
						mqttService.addClientFactory(appear.brokerName, new SecureClientFactory(appear));
						
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return false;
			}

			@Override
			public boolean disappear(Object o) {
				return false;
			}

		});
		
		
		/*new Timer().scheduleAtFixedRate(new TimerTask() {
			int idx = 0;
			@Override
			public void run() {
				mqttService.send(new Message(idx++).setQos(1), "default/hahah");
			}
		}, 1000, 1000);*/

	}

	public void stop(BundleContext bundleContext) throws Exception {
		mqttServiceRegistration.unregister();
		Activator.context = null;
	}

}

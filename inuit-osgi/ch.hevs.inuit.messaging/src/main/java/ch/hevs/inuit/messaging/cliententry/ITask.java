package ch.hevs.inuit.messaging.cliententry;

public interface ITask {
	public void run();
}

package ch.hevs.inuit.messaging;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

public class SecureClientFactory implements IClientFactory {
	SecurePresence presence;

	public SecureClientFactory(SecurePresence presence) {
		this.presence = presence;
		
	}
	/**
	 * IClientFactory
	 */
	@Override
	public MqttClient newClient() throws MqttException {
		MqttClient client = new MqttClient("ssl://" + presence.ip, MqttClient.generateClientId(),null);
		return client;
	}
 

	/**
	 * IClientFactory
	 */
	@Override
	public void connect(MqttClient client) throws MqttSecurityException, MqttException {
		client.connect(getOptions());
	}

	public MqttConnectOptions getOptions() {
		MqttConnectOptions options = new MqttConnectOptions();
		try {
			options.setSocketFactory(getSocketFactory(presence.cafile, presence.certfile,
					presence.keyfile, presence.certpassword));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		options.setCleanSession(true);
	
//		if (user != null)
//			options.setUserName(user);
//		if (password != null)
//			options.setPassword(password.toCharArray());
		

		return options;
	}

	
	
	static SSLSocketFactory getSocketFactory(final String caCrtFile,
			final String crtFile, final String keyFile, final String password)
			throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		// load CA certificate
		PEMReader reader = new PEMReader(new InputStreamReader(
				new ByteArrayInputStream(Files.readAllBytes(Paths
						.get(caCrtFile)))));
		X509Certificate caCert = (X509Certificate) reader.readObject();
		reader.close();

		// load client certificate
		reader = new PEMReader(new InputStreamReader(new ByteArrayInputStream(
				Files.readAllBytes(Paths.get(crtFile)))));
		X509Certificate cert = (X509Certificate) reader.readObject();
		reader.close();

		// load client private key
		reader = new PEMReader(new InputStreamReader(new ByteArrayInputStream(
				Files.readAllBytes(Paths.get(keyFile)))), new PasswordFinder() {
			@Override
			public char[] getPassword() {
				return password.toCharArray();
			}
		});
		KeyPair key = (KeyPair) reader.readObject();
		reader.close();

		// CA certificate is used to authenticate server
		KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
		caKs.load(null, null);
		caKs.setCertificateEntry("ca-certificate", caCert);
		TrustManagerFactory tmf = TrustManagerFactory
				.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(caKs);

		// client key and certificates are sent to server so it can authenticate
		// us
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(null, null);
		ks.setCertificateEntry("certificate", cert);
		ks.setKeyEntry("private-key", key.getPrivate(), password.toCharArray(),
				new java.security.cert.Certificate[] { cert });
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
				.getDefaultAlgorithm());
		kmf.init(ks, password.toCharArray());

		// finally, create SSL socket factory
		SSLContext context = SSLContext.getInstance("TLSv1");
		context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		return context.getSocketFactory();
	}

}

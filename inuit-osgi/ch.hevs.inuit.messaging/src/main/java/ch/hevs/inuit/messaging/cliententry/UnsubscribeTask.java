package ch.hevs.inuit.messaging.cliententry;

import org.eclipse.paho.client.mqttv3.MqttException;

public class UnsubscribeTask implements ITask{

	public UnsubscribeTask(ClientEntry c, String topic) {
		this.c = c;
		this.topic = topic;
	}
	
	ClientEntry c;
	String topic;
	
	@Override
	public void run() {
		Integer count = c.subscriptions.get(topic);
		if (count == null)
			return;
		count--;
		if (count == 0) {
			c.subscriptions.remove(topic);
		} else {
			c.subscriptions.put(topic, count);
		}


		try {
			c.client.unsubscribe(topic);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

}

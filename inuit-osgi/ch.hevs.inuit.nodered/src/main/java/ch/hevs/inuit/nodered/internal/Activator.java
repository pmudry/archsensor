package ch.hevs.inuit.nodered.internal;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;
import ch.hevs.inuit.library.presence.NamedPresence;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public final class Activator extends DeviceActivatorHelper {
    private static final Logger log = LoggerFactory.getLogger(Activator.class);

    private Process nodeRed = null;
    private BundleContext context;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        context = bundleContext;
        exportResources("nodered/nodes", "/usr/local/lib/node_modules/node-red/nodes");
        super.start(bundleContext);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        super.stop(bundleContext);
    }

    @Override
    protected boolean appear(Object object) {
        NamedPresence appear = getAppear(object, "nodered", NamedPresence.class);
        if (appear != null && nodeRed == null) {
            try {
                log.info("Starting Node-Red");
                nodeRed = Runtime.getRuntime().exec("node-red");
                return true;
            } catch (IOException exception) {
                log.error(exception.getMessage());
            }
        }
        return false;
    }

    @Override
    protected boolean disappear(Object object) {
        if (getAppear(object, "nodered", NamedPresence.class) != null) {
            shutdown();
        }
        return true;
    }

    private void shutdown() {
        if (nodeRed != null) {
            log.info("Stopping Node-Red");
            nodeRed.destroy();
            nodeRed = null;
        }
    }

    private void exportResources(String rootPath, String targetFolder) {
        try {
            final JarFile jar = new JarFile(new File(new File(".").getAbsolutePath() + context.getBundle().getLocation().replace("file:", "/")));
            final Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                final JarEntry entry = entries.nextElement();
                if (entry.getName().startsWith(rootPath)) {
                    if (entry.isDirectory()) {
                        new File(targetFolder + "/" + entry.getName()).mkdir();
                    } else {
                        final InputStream input = jar.getInputStream(entry);
                        final OutputStream output = new FileOutputStream(new File(targetFolder + "/" + entry.getName().replace(rootPath, "")));
                        byte[] buf = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = input.read(buf)) > 0) {
                            output.write(buf, 0, bytesRead);
                        }
                        input.close();
                        output.close();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

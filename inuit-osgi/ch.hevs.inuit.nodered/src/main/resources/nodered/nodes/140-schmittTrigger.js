//Require main module
module.exports = function(RED) {

	function SchmittTriggerNode(n) {
		RED.nodes.createNode(this,n);
		var node = this;

		var state = n.default === 'true';

	    this.on("input", function(msg) {		
			var inputValue = parseInt(msg.payload);
			if(state == true){
				if(inputValue < n.fall){
					setState(false);
				}
			} else {
				if(inputValue > n.rise){
					setState(true);
				}
			}
		});

		function setState(next){
			if(next != state){
				state = next;
				publishState();
			}
		}

		function publishState(){
			var out = {};
			out.payload = state;
			out.qos = 1;
			out.retain = true;
			node.send(out);
		}
	}
	 
	RED.nodes.registerType("schmittTrigger",SchmittTriggerNode);
}

//Require main module
module.exports = function(RED) {
    var cron = require("cron");
	function AnalogFilterNode(n) {
		RED.nodes.createNode(this,n);
		var node = this;

		var state = 0.0;
		var input = 0.0;
		var factor = n.periode/n.tao;

	    this.on("input", function(msg) {		
			input = parseFloat(msg.payload);
		});

		this.interval_id = setInterval( function() {
			state = state + (input-state)*factor;
			publishState(state);
		}, n.periode * 1000 );


		function publishState(){
			var out = {};
			out.payload = state;
			out.qos = 1;
			out.retain = true;
			node.send(out);
		}

        this.on("close", function() {
            clearInterval(this.interval_id);
        });
	}
	 
	RED.nodes.registerType("analogFilter",AnalogFilterNode);
}

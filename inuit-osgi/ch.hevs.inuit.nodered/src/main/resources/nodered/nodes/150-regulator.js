//Require main module
module.exports = function(RED) {

	function RegulatorNode(n) {
		RED.nodes.createNode(this,n);
		var node = this;


		var feedBack = 0.0;
		var order = 0.0;

		var state = 0;
		var error = 0;
		var errorLast = 0;
		var integrator = 0;
		var derivate = 0;

	    this.on("input", function(msg) {
			if(msg.topic === "feedback"){		
				feedBack = parseFloat(msg.payload);
			}
			if(msg.topic === "order"){		
				order = parseFloat(msg.payload);
			}
		});

		this.interval_id = setInterval( function() {
			error = order-feedBack;
			integrator = integrator + error;
			derivate = (error-errorLast) / n.periode;
			state = error*n.P + integrator*n.I + derivate*n.D;
			errorLast = error;
			publishState();
		}, n.periode * 1000 );


		function publishState(){
			var out = {};
			out.payload = state;
			out.qos = 1;
			out.retain = true;
			node.send(out);
		}

        this.on("close", function() {
            clearInterval(this.interval_id);
        });
	}
	 
	RED.nodes.registerType("regulator",RegulatorNode);
}

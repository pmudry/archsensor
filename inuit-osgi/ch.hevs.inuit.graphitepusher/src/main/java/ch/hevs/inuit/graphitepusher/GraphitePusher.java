package ch.hevs.inuit.graphitepusher;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.HashSet;
import java.util.concurrent.LinkedBlockingQueue;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

/**
 * Listen a topic/# that give registration request For example
 * topic/default/sensor/temperature mean that at default/sensor/temperature
 * there is a /data that want to be pushed on the graphite server.
 */
public class GraphitePusher {

	public GraphitePusher(IMessagingService messaging, GraphitePusherAppear appear) {
		this.messaging = messaging;
		this.appear = appear;

		// Boot the ThreadPusher
		pusher = new ThreadPusher();
		pusher.start();

		// Subscribe to the registration topic
		messaging.subscribe(new IMessagingSubscriber() {
			@Override
			public void receive(String topic, Message message) {
				registrationReceive(topic, message);
			}
		}, appear.topic + "/#");
	}

	IMessagingService messaging;
	GraphitePusherAppear appear;

	ThreadPusher pusher;
	LinkedBlockingQueue<Task> pendingTask = new LinkedBlockingQueue<Task>(1000);

	HashSet<String> registrations = new HashSet<String>(); // Avoid multiple
															// registration on
															// same topic

	/**
	 * Pushing request on graphite
	 */
	class Task {
		public Task(String line) {
			this.line = line;
		}
		String line;
	}

	/**
	 * The pushing is done by a dedicated thread that is fed by pendingTask
	 * queue
	 */
	class ThreadPusher extends Thread {

		ThreadPusher() {
		}

		// Socket socket;

		public void run() {
			// Wait task and push it
			while (true) {	
				Socket socket = null;
				try {
					Thread.sleep(5000);
					socket = new Socket(appear.ip, appear.port);
					Writer writer = new OutputStreamWriter(socket.getOutputStream());
					while(pendingTask.isEmpty() == false){
						Task t = pendingTask.take();
						writer.write(t.line);
					}
					writer.flush();
					writer.close();
				} catch (Exception e) {
					if(socket != null){
						try {
							socket.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}

			}
		}
	}

	// Messaging callback for registration
	public void registrationReceive(String topic, Message message) {
		topic = topic.substring(appear.topic.length() + 1, topic.length());

		if (registrations.contains(topic)) // avoid to register multiple time
			return;

		registrations.add(topic);
		messaging.subscribe(new IMessagingSubscriber() {
			@Override
			public void receive(String topic, Message message) {
				if (topic.endsWith("/data")) {
					String json = message.getPayloadString().replace(" ", "");
					int start = json.indexOf("\"v\":[") + 5;
					int end = json.indexOf("]",start);
					String value = json.substring(start,end);
					String str = topic.replace("/", ".") + " " + value + " " + (System.currentTimeMillis()/1000) + "\n";
					pushTask(new Task(str));
				}
			}
		}, topic + "/+");

	}

	protected void pushTask(Task task) {
		pendingTask.add(task);
	}
}

package ch.hevs.inuit.graphitepusher;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;

public class Activator extends DeviceActivatorHelper {
	@Override
	protected boolean appear(Object o) {
		try {

			GraphitePusherAppear appear = getAppear(o, "graphitePusher",GraphitePusherAppear.class);
			if (appear != null) {
				System.out.println("new GraphitePusher " + appear.topic);
				new GraphitePusher(messagingService, appear);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object o) {
		return false;
	}

}

package ch.hevs.inuit.graphitepusher;

import ch.hevs.inuit.library.presence.NamedPresence;

public class GraphitePusherAppear extends NamedPresence{
	public String ip;
	public Integer port;
	public String topic;
	
	@Override
	public boolean check() {
		return super.check() && ip != null  && port != null  && topic != null;
	}
}

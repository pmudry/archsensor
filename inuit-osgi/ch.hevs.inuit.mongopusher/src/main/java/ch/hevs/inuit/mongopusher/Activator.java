package ch.hevs.inuit.mongopusher;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;

public class Activator extends DeviceActivatorHelper {
	@Override
	protected boolean appear(Object o) {
		try {

			MongoPusherAppear appear = getAppear(o, "mongoPusher",MongoPusherAppear.class);
			if (appear != null) {
				System.out.println("new MongoPusher " + appear.topic);
				new MongoPusher(messagingService, appear);
				
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object o) {
		return false;
	}

}

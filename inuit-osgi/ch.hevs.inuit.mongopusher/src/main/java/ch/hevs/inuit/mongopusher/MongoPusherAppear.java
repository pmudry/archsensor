package ch.hevs.inuit.mongopusher;

import ch.hevs.inuit.library.presence.NamedPresence;

public class MongoPusherAppear extends NamedPresence{
	public String ip;
	public String database;
	public String topic;
	
	@Override
	public boolean check() {
		return super.check() && ip != null  && database != null  && topic != null;
	}
}

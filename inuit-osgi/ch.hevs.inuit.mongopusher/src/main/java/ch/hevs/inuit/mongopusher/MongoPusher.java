package ch.hevs.inuit.mongopusher;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/**
 * Listen a topic/# that give registration request For example
 * topic/default/sensor/temperature mean that at default/sensor/temperature
 * there is a /data + /model that want to be pushed on the cloud.
 */
public class MongoPusher {

	public MongoPusher(IMessagingService messaging, MongoPusherAppear appear) {
		this.messaging = messaging;
		this.appear = appear;

		// Boot the ThreadPusher
		pusher = new ThreadPusher();
		pusher.start();

		// Subscribe to the registration topic
		messaging.subscribe(new IMessagingSubscriber() {
			@Override
			public void receive(String topic, Message message) {
				registrationReceive(topic, message);
			}
		}, appear.topic + "/#");
	}

	IMessagingService messaging;
	MongoPusherAppear appear;

	ThreadPusher pusher;
	LinkedBlockingQueue<Task> pendingTask = new LinkedBlockingQueue<Task>(100);

	HashSet<String> registrations = new HashSet<String>(); // Avoid multiple
															// registration on
															// same topic
	HashSet<String> models = new HashSet<String>(); // Avoid multiple mongodb
													// push of same model

	/**
	 * Pushing request on mongodb
	 */
	class Task {
		public Task(String collectionName, String payload) {
			this.collectionName = collectionName;
			this.payload = payload;
		}

		String collectionName;
		String payload;
	}

	/**
	 * The pushing is done by a dedicated thread that is fed by pendingTask
	 * queue
	 */
	class ThreadPusher extends Thread {

		ThreadPusher() {
			// Connect to mongodb
			try {
				mongoClient = new MongoClient(appear.ip);
				db = mongoClient.getDB("inuit");
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		MongoClient mongoClient;
		DB db;

		public void run() {
			// Wait task and push it
			while (true) {
				try {
					Task t = pendingTask.take();
					DBObject doc = (DBObject) JSON.parse(t.payload);
					DBCollection coll = db.getCollection(t.collectionName);
					coll.insert(doc);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Messaging callback for registration
	public void registrationReceive(String topic, Message message) {
		topic = topic.substring(appear.topic.length() + 1, topic.length());
		MongoPublishRegistration registrationMessage;
		try {
			registrationMessage = Json.to(message, MongoPublishRegistration.class);
		} catch (Exception e) {
			registrationMessage = new MongoPublishRegistration();
		}
		
		
		if (registrations.contains(topic)) //avoid to register multiple time
			return;

		
//		if(registrationMessage.id == null){
//			InetAddress ip;
//			try {
//				ip = InetAddress.getLocalHost();
//				NetworkInterface network = NetworkInterface.getByInetAddress(ip);
//				byte[] mac = network.getHardwareAddress();
//
//				UUID id = UUID.fromString(topic + Arrays.toString(mac));
//				UUID id = UUID.fromString(model);
//				registrationMessage.id = id;
//			} catch (Exception e) {
//				return;
//			}
//		}
		
		registrations.add(topic);
		messaging.subscribe(new IMessagingSubscriber() {
			
			private MongoPublishRegistration registration;
			IMessagingSubscriber constructor(MongoPublishRegistration registration){
				this.registration = registration;
				return this;
			}
			@Override
			public void receive(String topic, Message message) {			
				if (topic.endsWith("/model")) {
					String model = message.getPayloadString();
					if (models.contains(model))
						return;
					models.add(model);
					
					if(registration.id == null)
						registration.id = UUID.nameUUIDFromBytes(model.getBytes());
						
					model = insert(model,"id","\"" + registration.id.toString() + "\"");
					pushTask(new Task("model", model));
				}
				if (topic.endsWith("/data")) {
					if(registration.id == null) return;
					String data = message.getPayloadString();
					data = insert(data,"id","\"" + registration.id.toString() + "\"");
					pushTask(new Task("data", data));
				}
			}
			
			String insert(String message,String key,String value){
				return message.replaceFirst("\\{", "{\"" + key +"\":" + value + ",");
			}
		}.constructor(registrationMessage), topic + "/+");

	
	}

	protected void pushTask(Task task) {
		pendingTask.add(task);
	}
}

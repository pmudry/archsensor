package ch.hevs.inuit.presencemanager;

import java.util.HashSet;

import ch.hevs.inuit.library.service.IPresenceManager;
import ch.hevs.inuit.library.service.IPresenceObserver;

public class PresenceManager implements IPresenceManager {
	HashSet<IPresenceObserver> observers = new HashSet<IPresenceObserver>();

	@Override
	public void subscribe(IPresenceObserver observer) {
		observers.add(observer);
	}

	@Override
	public void unsubscribe(IPresenceObserver observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyAppear(Object o) {
		int count = 0;
		for (IPresenceObserver observer : observers) {
			if (observer.appear(o)) {
				count++;
			}
		}
		if (count == 0) {
			System.err.println("notifyAppear, nobody interested in " + o);
		}
	}

	@Override
	public void notifyDisappear(Object o) {
		int count = 0;

		for (IPresenceObserver observer : observers) {
			if (observer.disappear(o)) {
				count++;
			}
		}
		if (count == 0) {
			System.err.println("notifyDisappear, nobody interested in " + o);
		}
	}
}

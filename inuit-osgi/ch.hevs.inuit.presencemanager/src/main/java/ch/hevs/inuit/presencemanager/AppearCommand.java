package ch.hevs.inuit.presencemanager;

public class AppearCommand {
    private PresenceManager manager;

    public AppearCommand(PresenceManager manager) {
       this.manager = manager;
    }

    public void appear(String json) {
        manager.notifyAppear(json);
    }

    public void disapper(String name) {
        manager.notifyDisappear("{\"name\":\"" + name + "\"");
    }
}

package ch.hevs.inuit.presencemanager.internal;

import ch.hevs.inuit.presencemanager.AppearCommand;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import ch.hevs.inuit.library.service.IPresenceManager;
import ch.hevs.inuit.presencemanager.PresenceManager;

import java.util.Hashtable;

public class Activator implements BundleActivator {
	private BundleContext context;

	PresenceManager presenceManager;

	public void start(BundleContext bundleContext) throws Exception {
		context = bundleContext;

		presenceManager = new PresenceManager();
		bundleContext.registerService(IPresenceManager.class.getName(),presenceManager, null);

		// Register presence manager command at felix gogo shell.
		Hashtable<String,Object> props = new Hashtable<>();
		props.put("osgi.command.scope", "inuit");
		props.put("osgi.command.function", new String[] {"appear"});
		context.registerService(AppearCommand.class.getName(), new AppearCommand(presenceManager), props);
	}

	public void stop(BundleContext bundleContext) throws Exception {

		context = null;
	}

}

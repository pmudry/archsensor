package ch.hevs.inuit.presencefilereader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.hevs.inuit.library.service.IPresenceManager;


/**
 * This bundle push on presence manager each json string contained in the presenceFile.json 
 */
public class Activator implements BundleActivator {
	private static Logger log = LoggerFactory.getLogger(Activator.class);
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}
	

	public void start(BundleContext bundleContext) throws Exception {
		log.info("[PresenceFile] START");
		Activator.context = bundleContext;
		
		ServiceReference presenceManagerReference = bundleContext.getServiceReference(IPresenceManager.class.getName());
		IPresenceManager presenceManager = (IPresenceManager) bundleContext.getService(presenceManagerReference);

		// Find the input stream for the boot file.
		InputStream presenceFileInput = null;
		
		String presenceFileLocation = bundleContext.getProperty(getClass().getPackage().getName() + ".file");
		if (presenceFileLocation != null) {
			try {
				presenceFileInput = getResource("file://" + presenceFileLocation);
				log.info("Loaded presence file:\"" + presenceFileLocation + "\"...");
			} catch (IOException exception) {
				log.warn("Presence file \"" + presenceFileLocation + "\" not found!");
			} catch (URISyntaxException exception) {
				log.warn("\"" + presenceFileLocation + "\" is an invalid file URI!");
			}
		}
		
		if (presenceFileInput == null) {
			Path path = Paths.get(System.getProperty("user.home") + "/.config/inuit/bootstrap.json");
			try {
				presenceFileInput = getResource(path.toUri().toString());
				log.info("Loaded presence file:\"" + path.toString() + "\"...");
			} catch (FileNotFoundException exception) {
			} catch (URISyntaxException exception) {
			}
		}

		if (presenceFileInput == null) {
			Path path = Paths.get("/etc/inuit/bootstrap.json");
			try {
				presenceFileInput = getResource(path.toUri().toString());
				log.info("Loaded presence file:\"" + path.toString() + "\"...");
			} catch (FileNotFoundException exception) {
			} catch (URISyntaxException exception) {
			}
		}
		
		if (presenceFileInput == null) {
			String defaultLocation = "bundle:/bootstrap.json";
			try {
				presenceFileInput = getResource(defaultLocation);
				log.info("Loading presence file:\"" + defaultLocation + "\"...");
			} catch (FileNotFoundException exception) {
				log.error("Default presence file \"" + defaultLocation + "\" not found! No presence file will be used!");
			} catch (URISyntaxException exception) {
				log.error("\"" + defaultLocation + "\" is an invalid file URI, No presence file will be used!");
			}
		}
        
        if (presenceFileInput == null) {
            log.error("Fatal error while loading presence definition file. Aborting presence activations!");
            return;
        }
        
		// Load file.
		byte[] data = null;
		if (presenceFileInput != null) {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int nRead;
			byte[] tmp = new byte[16384];
			while ((nRead = presenceFileInput.read(tmp, 0, tmp.length)) != -1) {
				buffer.write(tmp, 0, nRead);
			}
			buffer.flush();
			data = buffer.toByteArray();
		} 

		String presenceString = new String(data);
		int stack = 0;
		int start = 0;
		int idx = 0;
		for(char c : presenceString.toCharArray()){
			if(c == '{'){
				if(stack == 0){
					start = idx;
				}
				stack++;
			}
			if(c == '}'){
				stack--;
				if(stack == 0){ //push it ?

					String presence = presenceString.substring(start, idx+1);
					System.out.println(presence);
					presenceManager.notifyAppear(presence);
				}
			}

			idx++;
		}
		
		idx = 0;
		System.out.println("[PresenceFile] DONE");
	}


	public void stop(BundleContext bundleContext) throws Exception {

		Activator.context = null;
	}

	private InputStream getResource(String location) throws URISyntaxException, IOException {
		URI uri = new URI(location);
		if (uri.getScheme() != null){
			if (uri.getScheme().equals("classpath")) {
				return getClass().getClassLoader().getResourceAsStream(uri.getSchemeSpecificPart());
			} else if (uri.getScheme().equals("bundle")) {
				URL url = context.getBundle().getEntry(uri.getSchemeSpecificPart());
				if (url != null) {
					URLConnection connection = url.openConnection();
					if (connection != null) {
						return connection.getInputStream();
					} else {
						return null;
					}
				} else {
					return null;
				}
			} else {
				Path path = Paths.get(uri);
				File file = path.toFile();
				return new FileInputStream(file);
			}
		} else {
			throw new URISyntaxException(location, "invalid Scheme");
		}
	}
}

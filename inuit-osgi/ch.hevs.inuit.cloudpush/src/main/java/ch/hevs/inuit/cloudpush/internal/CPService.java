package ch.hevs.inuit.cloudpush.internal;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import ch.hevs.inuit.cloudpush.CPServiceDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

/**
 * iNUIT cloud push service implementation.
 *
 * @author Michael Clausen (michael dot clausen at hevs dot ch)
 *
 */
public class CPService implements IMessagingSubscriber {
	private static Logger log = LoggerFactory.getLogger(CPService.class);
	
	// Messaging API.
	private IMessagingService messaging;
	
	// Service descriptor and base URL as URI.
	private CPServiceDescriptor descriptor;
	private URI baseUri;
	
	// All MQTT subscriptions to be pushed to the cloud by this service.
	private Set<CPSubscription> subscriptions = new TreeSet<CPSubscription>();
	
	// Detached thread that actually executes the HTTP jobs in order to push the data to the iNUIT datastore.
	private CPHttpJobExecutor httpJobExecutor = new CPHttpJobExecutor();
	
	/**
	 * Constructor, constructs and prepares the cloud push service.
	 * @param messaging				The messaging API object to use to subscribe to topics.
	 * @param descriptor			The service descriptor.
	 * @throws URISyntaxException	This exception is thrown if the baseUrl in the service descriptor is invalid.
	 */
	public CPService(IMessagingService messaging, CPServiceDescriptor descriptor) throws URISyntaxException {
		
		// Save properties.
		this.messaging = messaging;
		this.descriptor = descriptor;
		
		// Try to parse the RESTful webservice base URL.
		this.baseUri = new URI(descriptor.baseUrl);
		
		// Subscribe to all leaves under the registration topic.
		messaging.subscribe(this, descriptor.topic + "/#");
	}

	/**
	 * Returns the base URI the cloud push service is pushing data to.
	 * @return	Base URL of the cloud datastore RESTful web service.
	 */
	public URI getBaseUri() {
		return baseUri;
	}

	/**
	 * Returns the HTTP job executor which will to be used for all HTTP requests.
	 * @return	HTTP job executor object.
	 */
	public CPHttpJobExecutor getHttpJobExecutor() {
		return httpJobExecutor;
	}
	
	// IMessagingSubscriber implementation.
	@Override
	public void receive(String topic, Message message) {
		// The actual subscription topic can be determined by removing the actual registration root topic.
		String subscriptionTopic = topic.substring(descriptor.topic.length() + 1);
		if (!subscriptionTopic.isEmpty()) {
			
			// The message payload has to be the UUID of the sensor.
			UUID sensorUuid = UUID.fromString(message.getPayloadString());
			if (sensorUuid != null) {
				// Create a new subscription object and add it to the set of subscriptions if the topic is not already registered.
				CPSubscription subscription = new CPSubscription(this, subscriptionTopic, sensorUuid);
				if (!subscriptions.contains(subscription)) {
					subscriptions.add(subscription);
					
					// Activate the subscription.
					subscription.activate(messaging);
					
					log.info("New cloud push subscription to topic:\"" + subscriptionTopic + "\"");
					
					log.info("Added topic \"" + subscriptionTopic + "\" subscription for /model and /data.");
				} else {
					log.warn("Topic \"" + subscriptionTopic + "\" is already in use.");
				}
			} else {
				log.error("Missing or invalid sensor UUID, ignoring subscription!");
			}
			
		} else {
			log.error("Empty topic not allowed. Ignoring.");
		}
	}
}

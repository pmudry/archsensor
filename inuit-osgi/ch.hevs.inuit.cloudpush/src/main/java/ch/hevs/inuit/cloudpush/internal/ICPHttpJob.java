package ch.hevs.inuit.cloudpush.internal;

import org.apache.http.impl.client.CloseableHttpClient;

/**
 * Interface for all HTTP jobs that can be executed by the CPHttpJobExecutor class.
 * 
 * @see CPHttpJobExecutor.
 * @author Michael Clausen (michael dot clausen at hevs dot ch) 
 */
interface ICPHttpJob {
	/**
	 * This method is called by the HTTP job executor in order to run the actual job. The method has to return true if the 
	 * job succeeded or if the job failed but does not have to be retried later. If the job failed and has to be retried 
	 * afterwards, the method should return false.
	 * 
	 * @param client	The HTTP job executor's HTTP client to use in order to execute the job.
	 * @return			True on success or failure without retry, false on job fail when a retry of the job is needed.
	 */
	boolean execute(CloseableHttpClient client);
	
	/**
	 * Sets a listener that gets informed about the job execution (and if failed or not).
	 * @param listener
	 */
	void setListener(ICPHttpJobListener listener);
}

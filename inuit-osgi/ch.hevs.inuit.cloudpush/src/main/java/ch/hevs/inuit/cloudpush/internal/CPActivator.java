package ch.hevs.inuit.cloudpush.internal;

import java.net.URISyntaxException;

import ch.hevs.inuit.cloudpush.CPServiceDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;

/**
 * Activator for the iNUIT Cloud Push Service.
 * <br>
 * Registers the OSGi bundle within the OSGi environment and subscribes for new appear messages. If a message with the name "cloudPusher"
 * has been received by the presence manager, a new cloud push service will be instantiated. 
 * <br>
 * The message has to contain the <b>"baseUrl"</b> of the RESTful web-service (excluding the version string of the service, this will be 
 * added by the service automatically according to the actual version used by the push service). 
 * <br>
 * The second mandatory parameter of the message is "topic". This registration service topic will be used in order to register topics 
 * that are subject to be pushed to the cloud. 
 * <br>
 * So if a sensor's model and data needs to be pushed to the iNUIT cloud data store, all to do is to send a message to the topic of the 
 * following format:
 * <br>
 * <b>[Cloud Push Service Topic]/[Sensor Topic]</b>
 * Sensor Topic is the base topic of the sensor excluding /model and /data, the service will automatically subscribe to both of them. So 
 * for example if a sensor publishing its model to /some/example/topic/model and data updates to /some/example/topic/data should be pushed
 * to the cloud (service is using registration topic /default/cloud) a message containing only the UUID (version 4) of the sensor has to be 
 * sent to the following topic:
 * <br>
 * <b>/default/cloud/some/example/topic</b>
 * <br>
 * It is important that the message content is just a string representation of a valid UUID (version 4) for the sensor.
 * After that all data send to the topics /some/example/topic/{model, data} will be automatically pushed to the cloud. If the message does 
 * not contains already an id property, the service will add an UUID generated using the actual model JSON string of the sensor.
 *  
 * @author Michael Clausen (michael dot clausen at hevs dot ch)
 */
public class CPActivator extends DeviceActivatorHelper {
	static Logger log = LoggerFactory.getLogger(CPActivator.class);
	
	@Override
	protected boolean appear(Object obj) {
		// Check if the presence manager message's name is "cloudPusher" and get the descriptor of the service.
		CPServiceDescriptor descriptor = getAppear(obj, "cloudPusher", CPServiceDescriptor.class);
		if (descriptor != null) {
			try {
				// Instantiate the new cloud push service.
				new CPService(messagingService, descriptor);
				log.info("New service created listening to topic: " + descriptor.topic + " using base URL " + descriptor.baseUrl);
				return true;
			} catch (URISyntaxException exception) {
				log.error("Failed to create new service. Reason: " + exception.getMessage());
			}
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object obj) {
		log.error("Service does not implelent disappereance!");
		return false;
	}
}

package ch.hevs.inuit.cloudpush.internal;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executes HTTP request in a detached thread.
 *
 * @author Michael Clausen (michael dot clausen at hevs dot ch)
 */
public class CPHttpJobExecutor extends Thread {
	private static Logger log = LoggerFactory.getLogger(CPHttpJobExecutor.class);
	
	// The HTTP client.
	private CloseableHttpClient client;
	
	// List of all pending jobs.
	private List<ICPHttpJob> jobs = new LinkedList<ICPHttpJob>();
	
	// Does the thread has to run?
	private boolean running = true;
	
	/**
	 * Constructor, creates a new HTTP job executor and starts the detached thread.
	 */
	public CPHttpJobExecutor() {
		// Set the HTTP timeout to a useful value.
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(250).setSocketTimeout(250).build();
		client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		
		// Start the detached thread.
		start();
	}
	
	/**
	 * Destructor alike.
	 */
	public void dispose() {
		try {
			// Close the HTTP client.
			client.close();
		} catch (IOException exception) {
			log.error("Error when shuting down HTTP client: " + exception.getMessage());
		}
		
		// Stop execution of HTTP executor thread.
		running = false;
		
		// If the thread is waiting, wake it up in order to leave its execution loop.
		jobs.notify();
	}
	
	/**
	 * Adds a new job to the end of the working queue of the HTTP job executor.
	 *
	 * @param job	Job to add.
	 */
	public void addJob(ICPHttpJob job) {
		synchronized(this) {
			// Do we need to wake the worker thread?
			boolean needsNotify = jobs.isEmpty();
			
			// Add the job to the end of the jobs list.
			jobs.add(job);
			
			// Do we need to wake up the thread?
			if (needsNotify) {
				notify();
			}
		}
	}

	// Thread implementation.
	@Override
	public void run() {
		while (running) {
			// If there are no jobs to execute, wait for a notification.
			if (jobs.isEmpty()) {
				synchronized (this) {
					try {
						wait();
					} catch (InterruptedException exception) {
						log.error("Failed to wait for notify: " + exception.getMessage());
					}
				}
			}
			
			// Re-check that the thread should run and there is at least one job in the queue.
			if (running && !jobs.isEmpty()) {
				// Get the first job out of the queue.
				ICPHttpJob job = jobs.remove(0);
				
				// Try to execute the job.
				if (!job.execute(client)) {
					// If the job fails and needs to be rescheduled, add the job and wait a moment as other jobs will
					// fail most probably too as the most probable problem is the connection to the server.
					log.warn("Job " + job.toString() + " failed, rescheduling job...");
					jobs.add(job);
					try {
						Thread.sleep(100);
					} catch (InterruptedException exception) {
						log.error("Fatal exception while pausing HTTP job executor: " + exception.getMessage());
					}
				}
			}
		}
	}
}

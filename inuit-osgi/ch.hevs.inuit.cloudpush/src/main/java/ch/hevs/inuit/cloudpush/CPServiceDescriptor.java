package ch.hevs.inuit.cloudpush;

import ch.hevs.inuit.library.presence.NamedPresence;

/**
 * Descriptor for the iNUIT cloud push service.
 * 
 * @author Michael Clausen (michael dot clausen at hevs dot ch) 
 */
public class CPServiceDescriptor extends NamedPresence {
	/**
	 * The base URL of the service in the form http://{host}:{port}/{base path}. Example: http://inuit.iict.ch.
	 * Note that the version of the RESTful web service is added by the cloud push service automatically, so it 
	 * is not part of the base URL. 
	 */
	public String baseUrl;
	
	/**
	 * The topic the service is actually listening to for push registrations.
	 */
	public String topic;
}

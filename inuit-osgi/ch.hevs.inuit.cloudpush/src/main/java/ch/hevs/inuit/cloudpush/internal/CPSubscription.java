package ch.hevs.inuit.cloudpush.internal;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;

import ch.hevs.inuit.library.json.Json;
import ch.hevs.inuit.library.service.IMessagingService;
import ch.hevs.inuit.library.service.IMessagingSubscriber;
import ch.hevs.inuit.library.service.Message;

/**
 * An actual sensor subscription which pushes the actual data to the cloud.
 *
 * @author Michael Clausen (michael dot clausen at hevs dot ch)
 */
public class CPSubscription implements IMessagingSubscriber, Comparable<CPSubscription> {
	private static Logger log = LoggerFactory.getLogger(CPSubscription.class);
	
	// The iNUIT cloud push service this subscription belongs to.
	private CPService service;
	
	// The UUID of the sensor (Automatically generated when the model of the sensor is send for the first time.
	private UUID uuid;
	
	// The base topic the sensor subscription is listening to.
	private String topic;
	
	// Does the sensor already send his model?
	private boolean modelSend = false;
	
	// Cached samples in order to be able to wait until model is send.
	private List<ICPHttpJob> sampleCache = new LinkedList<ICPHttpJob>();
	
	/**
	 * Creates a new sensor subscription object.
	 *
	 * @param service	Service which contains the subscription.
	 * @param topic		The base topic of the sensor.
	 */
	public CPSubscription(CPService service, String topic, UUID uuid) {
		this.service = service;
		this.topic = topic;
		this.uuid = uuid;
	}
	
	/**
	 * Activates (subscribes to the sensor's topics) the subscription.
	 *
	 * @param messaging	Messaging API object to use in order to subscribe to MQTT topics.
	 */
	public void activate(IMessagingService messaging) {
		// Subscribe to the model and the data topic of the sensor.
		messaging.subscribe(this, topic + "/model");
		messaging.subscribe(this, topic + "/data");
	}
	
	/**
	 * Deactivates (unsubscribes to the sensor's topics) the subscription.
	 *
	 * @param messaging	Messaging API object to use in order to unsubscribe from MQTT topics.
	 */
	public void deactivate(IMessagingService messaging) {
		// Unsubscribe from the model and the data topic of the sensor.
		messaging.unsubscribe(this, topic + "/model");
		messaging.unsubscribe(this, topic + "/data");
	}

	// IMessagingSubscriber implementation.
	@Override
	public void receive(String topic, Message message) {
		// If the topic ends with /data, the sensor actually send us data.
		if (topic.endsWith("/data")) {
			try {
				// Construct RESTful web service URL.
				URI base = service.getBaseUri();
				URI uri = new URIBuilder().setScheme(base.getScheme()).setHost(base.getHost()).setPort(base.getPort()).setPath(base.getPath() + "/1/msmts").build();
				
				// Get JSON object representation of the sensor message's payload and add the uuid as property id.
				JsonObject json = Json.parse(message.getPayloadString());
				json.addProperty("id", uuid.toString());
				
				// Create job.
				CPHttpPostJob job = new CPHttpPostJob(uri, json.toString(), true);

				if (modelSend) {
					// Add the push job to the HTTP job executor if model has been already send.
					service.getHttpJobExecutor().addJob(job);
				} else {
					if (sampleCache.size() < 32 ) {
						log.warn("Received sensor data for \"" + topic + "\" while sensor model is not known yet. Caching message.");
						
						// Add the push job to the sample cache that will be send once the model has been published.
						sampleCache.add(job);
					} else {
						log.warn("Received sensor data for \"" + topic + "\" while sensor model is not known yet. Caching message not possible as cache is full!");
					}
				}
			} catch (URISyntaxException exception) {
				log.error("Error building URI for data update: " + exception.getMessage() + " - Dropping message.");
			}
		} else if (topic.endsWith("/model") && !modelSend) {
			try {
				// Get the model of the sensor as string and construct UUID based on that string.
				String model = message.getPayloadString();
				
				// Get JSON object representation of the sensor data and add the sensor UUID as property id.
				JsonObject json = Json.parse(model);
				json.addProperty("id", uuid.toString());
				
				// Construct the the RESTful web service URL.
				URI base = service.getBaseUri();
				URI uri = new URIBuilder().setScheme(base.getScheme()).setHost(base.getHost()).setPort(base.getPort()).setPath(base.getPath() + "/1/sensors").build();
				
				// Add the push job to the HTTP job executor.
				CPHttpPostJob job = new CPHttpPostJob(uri, json.toString(), new CPHttpPostJob.RetryDecisionDelegate() {
					@Override
					public boolean shouldRetry(int status) {
						return status != 403;
					}
				});
				
				job.setListener(new ICPHttpJobListener() {
					@Override
					public void jobDone(ICPHttpJob job, int httpCode) {
						if ((httpCode >= 200 && httpCode < 300) || httpCode == 403)
						log.info("Model send, emtying sample cache.");
						modelSend = true;
						for (ICPHttpJob j: sampleCache) {
							service.getHttpJobExecutor().addJob(j);
						}
						sampleCache.clear();
					}
				});
				service.getHttpJobExecutor().addJob(job);
			} catch (URISyntaxException exception) {
				log.error("Error building URI for model update: " + exception.getMessage() + " - Dropping model data.");
			}
		}
	}

	// Needed in order to avoid duplicates of the same subscription within the iNUIT cloud push service.
	@Override
	public int compareTo(CPSubscription other) {
		return topic.compareTo(other.topic);
	}
}

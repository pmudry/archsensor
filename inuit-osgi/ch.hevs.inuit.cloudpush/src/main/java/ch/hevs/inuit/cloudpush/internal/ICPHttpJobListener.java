package ch.hevs.inuit.cloudpush.internal;

public interface ICPHttpJobListener {
	void jobDone(ICPHttpJob job, int httpCode);
}

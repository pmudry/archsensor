package ch.hevs.inuit.cloudpush.internal;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A HTTP POST job.
 *
 * @author Michael Clausen (michael dot clausen at hevs dot ch)
 */
public class CPHttpPostJob implements ICPHttpJob {
	private static Logger log = LoggerFactory.getLogger(CPHttpPostJob.class);
	
	// The HTTP post object to execute.
	private HttpPost post;
	
	// If true the job is retried in the case it fails, otherwise the job is dropped.
	private RetryDecisionDelegate retryDecision;
	
	// Job listener.
	private ICPHttpJobListener listener = null;
	
	// Interface for the HTTP post retry decision delegate.
  	public interface RetryDecisionDelegate {
	  	boolean shouldRetry(int status);
	}

	/**
	 * Creates a new HTTP POST job. If the job fails it will be retried just until it succeeds.
	 *
	 * @param uri		The URI of the RESTful web service to POST the message to.
	 * @param content	The actual content of the POST message as string.
	 */
	public CPHttpPostJob(URI uri, String content) {
		this(uri, content, true);
	}
	
	/**
	 * Creates a new HTTP POST job.
	 *
	 * @param uri			The URI of the RESTful web service to POST the message to.
	 * @param content		The actual content of the POST message as string.
	 * @param retryIfFails	If true the job will be retried if it fails, if false the job is dropped if it fails.
	 */
	public CPHttpPostJob(URI uri, String content, boolean retryIfFails) {
		this(uri, content, new RetryDecisionDelegate() {
  			@Override
			public boolean shouldRetry(int status) {
				return retryIfFails;
			}
		});
	}
	
	public CPHttpPostJob(URI uri, String content, RetryDecisionDelegate retryDecision) {
		post = new HttpPost(uri);
		post.setEntity(new StringEntity(content, ContentType.create("application/json")));
		
		// Save failure decision delegate.
		this.retryDecision = retryDecision;
	}
	
	// ICPHttpJob implementation.
	@Override
	public boolean execute(CloseableHttpClient client) {
		try {
			// Execute the HTTP POST request and get the response from the server.
			CloseableHttpResponse response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();
			
			// It it important to close the response.
			response.close();
			
			if (listener != null) {
				listener.jobDone(this, status);
			}
			
			// If status is success...
			if (status >= 200 && status < 300) {
				// ...return success.
				return true;
			} else {
				// In the case the server respond with an error, do we have to reschedule the job for a retry?
				if (retryDecision == null || retryDecision.shouldRetry(status)) {
					log.error("Job failed with status: " + response.getStatusLine().getReasonPhrase() + ". Retrying...");
					return false;
				} else
				{
					log.error("Job failed with exception: " + response.getStatusLine().getReasonPhrase() + ". Dropping job.");
					return true;
				}
			}
 		} catch (IOException exception) {
 			// In the case of an exception in the HTTP library, do we have to reschedule the job for a retry?
			if (retryDecision == null || retryDecision.shouldRetry(-1)) {
				log.error("Job failed with exception: " + exception.getMessage() + ". Retrying...");
				return false;
			} else
			{
				log.error("Job failed with exception: " + exception.getMessage() + ". Dropping job.");
				return true;
			}
		}
	}
	
	@Override
	public void setListener(ICPHttpJobListener listener) {
		this.listener = listener;
	}
	
	@Override
	public String toString() {
		return "HTTP POST to " + post.getURI().toString();
	}
}

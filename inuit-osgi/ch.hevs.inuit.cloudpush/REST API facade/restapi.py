from bottle import get, post, put, delete, run, request, abort
from pprint import pprint


sensors = []

@post('/1/sensors')
def sensor_post():
    print '*************************************************************************'
    print 'New sensor: ' + request.json['id']
    print 'content-type:' + request.content_type
    pprint(request.json)
    sensors.append(request.json)
    print ''
    return ""


@get('/1/sensors/<sensor_id>')
def sensor_get(sensor_id):
    print '*************************************************************************'
    print 'Sensor read: ' + sensor_id
    print 'content-type:' + request.content_type
    for sensor in sensors:
        if sensor['id'] == sensor_id:
            print str(sensor)
            print ''
            return str(sensor)
    abort(404, 'Not found.')
    print str('NOT FOUND!')
    print ''


@put('/1/sensors/<sensor_id>')
def sensor_put(sensor_id):
    print '*************************************************************************'
    print 'Sensor update: ' + sensor_id
    print 'content-type:' + request.content_type
    for i, sensor in enumerate(sensors):
        if sensor['id'] == sensor_id:
            print str(request.json)
            print ''
            sensors[i] = request.json
            return ''
    abort(404, 'Not found.')
    print str('NOT FOUND!')
    print ''


@delete('/1/sensors/<sensor_id>')
def sensor_delete(sensor_id):
    print '*************************************************************************'
    print 'Sensor delete: ' + sensor_id + ' NOT IMPLEMENTED'
    print 'content-type:' + request.content_type
    print ''

@post("/1/msmts")
def measure_post():
    print '*************************************************************************'
    print 'Sensor data: '
    print 'content-type:' + request.content_type
    pprint(request.json)
    print ''

run(host='localhost', port=8080)

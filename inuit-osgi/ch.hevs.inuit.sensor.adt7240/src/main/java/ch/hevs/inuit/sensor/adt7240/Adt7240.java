package ch.hevs.inuit.sensor.adt7240;

import java.io.IOException;

import ch.hevs.inuit.library.i2c.II2cDevice;
import ch.hevs.inuit.library.misc.IFloatSource;

public class Adt7240 implements IFloatSource {

	final II2cDevice device;

	public Adt7240(II2cDevice device) throws IOException {
		this.device = device;
	}

	@Override
	public float getFloatValue() {
		short temp = 0;
	
		byte[] buf = new byte[16];
		try {
			device.read(0x00, buf, 0, 2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		temp += S8toU8(buf[0]) << 8;
		temp += S8toU8(buf[1]);

		return (temp >> 3) * 0.0625f + 273.15f;
	}

	int S8toU8(int value) {
		return value < 0 ? value + 256 : value;
	}

}
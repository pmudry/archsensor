package ch.hevs.inuit.sensor.adt7240;

import org.osgi.framework.BundleContext;

import ch.hevs.inuit.library.helper.DeviceActivatorHelper;
import ch.hevs.inuit.library.helper.SenderPeriodicFloat;
import ch.hevs.inuit.library.i2c.II2cBus;
import ch.hevs.inuit.library.json.model.Accuracy;
import ch.hevs.inuit.library.json.model.Model;
import ch.hevs.inuit.library.json.model.Physical;
import ch.hevs.inuit.library.json.model.Refresh;
import ch.hevs.inuit.library.json.model.Sampling;
import ch.hevs.inuit.library.json.model.Software;
import ch.hevs.inuit.library.presence.I2cPeriodicAllInOneAppear;
import ch.hevs.inuit.library.service.Message;

public class Activator extends DeviceActivatorHelper {
	
	
	
	public static Model newModel(BundleContext context,double period){
		Model model = new Model()
			.setDescription("Basic temperature sensor")
			.setName("ADT7240");

		model.sampling = new Sampling()
			.setPhysical(new Physical.Temperature(253.15, 378.15))
			.setRefresh(new Refresh.Periodic().setPeriod(period))
			.setAccuracy(new Accuracy.Absolute(0.25,0.0078));
		
		model.software = Software.from(context);


		return model;
	}
	
	@Override
	protected boolean appear(Object o) {
		try {

			I2cPeriodicAllInOneAppear appear = getAppear(o, "ADT7240", I2cPeriodicAllInOneAppear.class);
			if (appear != null) {
				System.out.println("new ADT7240");

				
				Model model = newModel(getContext(),appear.period);
				model.fillWith(appear.model);							
				messagingService.send(new Message(model), appear.topic + "/model");
				
				Adt7240 adt7240 = new Adt7240(((II2cBus) directory.get(appear.busName)).getDevice(appear.address));

				new SenderPeriodicFloat(
						adt7240,
						(long) (appear.period * 1000), appear.topic + "/data",
						messagingService);
				
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	protected boolean disappear(Object o) {
		return false;
	}

}

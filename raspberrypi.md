## Guide to install and use iNUIT/ArchSensor gateway on Raspberry PI

    # sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | sudo tee -a /etc/apt/sources.list
    # sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
    # sudo apt-get update
    # sudo apt-get install oracle-java8-installer libunixsocket-java git maven mosquitto
    # wget http://node-arm.herokuapp.com/node_0.10.36_armhf.deb
    # sudo dpkg -i node_0.10.36_armhf.deb
    # sudo apt-get install build-essential python-dev python-rpi.gpio
    # sudo npm install -g --unsafe-perm node-red
    # git clone git://git.drogon.net/wiringPi
    # cd wiringPi && sudo ./build
    # git clone https://{user}@bitbucket.org/pmudry/archsensor.git
    # cd ArchSensor/inuit-osgi
    # mvn install
    # sudo mvn psx:provision
    
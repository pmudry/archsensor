/**
 * Lightsensor
 * 
 * ROHM BH1730FVC <-> bus pirate -> MQTT
 * 
 * Wiring bus pirate <-> sensor
 * 
 * 		GND <-> GND (3)
 * 		3v3 <-> VCC (1) + DVI (5)
 * 		SDA (MOSI) <-> SDA (4)
 * 		SCL (CLOCK) <-> SCL (6)
 * 
 * 		4.7k pullups from SDA and SCL to 3v3
 */

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class LightSensorMqtt {
	
	public static final int SOME_TIME_MS = 100;

	/**
	 * Buspirate commands
	 */
	public static final String init_commands[] = 
	{
		/* Reset */
		"#",
		
		/* Setup i2c @ 400kHz*/
		"m",
		"4",
		"4",
		
		/* Enable the power */
		"W",
		
		/* Setup continuous conversion */
		"[0x52 0x80 0x3]",
		
		/* 
		 * Read values
		 * 
		 * This one is the last and will be repeated for each measure
		 */
		"[0x52 0x94 [0x53 rrrr]",
	};

	
	
	public static void main(String[] args) throws IOException,
			InterruptedException, UnsupportedCommOperationException,
			PortInUseException, NoSuchPortException, MqttException {

		/**
		 * A small class for grouping MQTT parameters
		 */
        class MqttConfig
		{
	        String topic = "lightsensor";
	        int qos = 0;
	        String broker;
	        String clientId     = "DoItToday";
	        MemoryPersistence persistence = new MemoryPersistence();
		}
        MqttConfig mqttConfig = new MqttConfig();     
		
        /* Minimalist input check and help */
        if (args.length != 3) {
        	System.out.println("example parameters : tcp://localhost:1883 /dev/ttyUSB0 1000");
        	System.out.println("#1 : mqtt broker");
        	System.out.println("#2 : buspirate serial port");
        	System.out.println("#3 : delay in miliseconds (minimum 100)");
        	System.exit(1);
        }
        
        /* Get input as said in help */
        mqttConfig.broker = args[0];
		String serialPortName = args[1];
		int dt = Integer.parseInt(args[2]);

		/* COM port initialization */
		CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(serialPortName);
		SerialPort serialPort = (SerialPort) portId.open("LightSensorMqtt", 0);
		serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		PrintWriter serialOut = new PrintWriter(serialPort.getOutputStream());
		InputStream serialIn = serialPort.getInputStream();
		
		/* MQTT initialization */
		MqttClient mqttConnection = new MqttClient(mqttConfig.broker, mqttConfig.clientId, mqttConfig.persistence);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		mqttConnection.connect(connOpts);
		
		
		/* Init the buspirate and the light sensor*/
		for (String cmd : init_commands) {
			/* Send the command */
			serialOut.write(cmd);
			serialOut.write("\n");
			serialOut.flush();
			
			/* Wait some time*/
			Thread.sleep(SOME_TIME_MS);
			
			/* Get the response */
			while (serialIn.available() > 0) {
				byte[] readBuffer = new byte[serialIn.available()];
				serialIn.read(readBuffer);
				System.out.println("cmd : " + cmd);
				System.out.println("resp: " + new String(readBuffer));
			}
		}
		
		for (;;)
		{
			/* Send the last command */
			String cmd = init_commands[init_commands.length -1];
			serialOut.write(cmd);
			serialOut.write("\n");
			serialOut.flush();
			
			/* Wait some time */
			Thread.sleep(Math.max(dt, SOME_TIME_MS));
			
			/* Get and parse the response */
			while (serialIn.available() > 0) {

				/* Get all response lines */
				byte[] readBuffer = new byte[serialIn.available()];
				serialIn.read(readBuffer);
				String responses[] = new String(readBuffer).replace("\r", "").split("\n");
				
				/* Extract the data */
				int i = 0;
				int nums[] = new int[4];
				for (String r : responses) {
					if (r.startsWith("READ:")) {
						nums[i++] = Integer.parseInt(r.split("x")[1].trim(), 16);
					}
				}

				/* 
				 * Convert to lux
				 * 
				 * Formulae from the datasheet
				 */
				int DATA0 = nums[0+0] + (nums[0+1] <<8);
				int DATA1 = nums[2+0] + (nums[2+1] <<8);
				double Lx;
				double Gain = 1;
				double ITIME_ms = 2.7*(256-0xda);

				if (DATA1/DATA0<0.26)
				{
					Lx = ( 1.290*DATA0 - 2.733*DATA1 ) / Gain * 100 / ITIME_ms;
				}
				else if (DATA1/DATA0<0.55)
				{
					Lx = ( 0.795*DATA0 - 0.859*DATA1 ) / Gain * 100 / ITIME_ms;
				}
				else if (DATA1/DATA0<1.09)
				{
					Lx = ( 0.510*DATA0 - 0.345*DATA1 ) / Gain * 100 / ITIME_ms;
				}
				else if (DATA1/DATA0<2.13)
				{
					Lx = ( 0.276*DATA0 - 0.130*DATA1 ) / Gain * 100 / ITIME_ms;
				}
				else
				{
					Lx = 0;
				}

				/* Publish to the topic */
				String content = "" + Lx;
				MqttMessage message = new MqttMessage(content.getBytes());
				message.setQos(mqttConfig.qos);
	            mqttConnection.publish(mqttConfig.topic, message);
				System.out.println("luminosity: " + Lx);
			}
			
			mqttConnection.close();
		}

		/*
		 * In the ideal world, we should close the serial port and the MQTT
		 * connection.
		 * 
		 * serialPort.close();
		 * mqttConnection.close();
		 * 
		 */
	}
}
